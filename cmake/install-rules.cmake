install(
    TARGETS AIVctrl_exe
    RUNTIME COMPONENT AIVctrl_Runtime
)

if(PROJECT_IS_TOP_LEVEL)
  include(CPack)
endif()
