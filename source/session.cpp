#include <QApplication>
#include <QDesktopWidget>
#include <QDir>
#include <QXmlStreamWriter>

#include "session.hpp"

#include "helper.hpp"

namespace
{
enum class Tags
{
  Uuid = 0,
  Volume,
  PlayMode,
  RemainingTime,
  Left,
  Top,
  Width,
  Height,
  IconSize,
  States,
  Language,
  Status
};
const std::unordered_map<Tags, const char*> tags {
    {Tags::Uuid, "uuid"},
    {Tags::Volume, "volume"},
    {Tags::PlayMode, "playMode"},
    {Tags::RemainingTime, "remainingTime"},
    {Tags::Left, "left"},
    {Tags::Top, "top"},
    {Tags::Width, "width"},
    {Tags::Height, "height"},
    {Tags::IconSize, "iconSize"},
    {Tags::States, "states"},
    {Tags::Language, "language"},
    {Tags::Status, "status"}};
}  // namespace

CSession::CSession(const QString& renderer,
                   int volume,
                   int playMode,
                   bool remainingTime,
                   const QRect& rect,
                   Qt::WindowStates windowStates,
                   int iconSize,
                   const QString& language,
                   unsigned status)
    : m_renderer {renderer}
    , m_volume {volume}
    , m_playMode {playMode}
    , m_remainingTime {remainingTime}
    , m_rect {rect}
    , m_windowStates {windowStates}
    , m_iconSize {iconSize}
    , m_language {language}
    , m_status {status}
{
}

QString CSession::standardFilePath()
{
  QString folder {appDataDirectory()};
  if (!folder.isEmpty()) {
    QDir dir {folder};
    return dir.absoluteFilePath(m_fileName);
  }

  return {};
}

bool CSession::characters(const QString& name)
{
  const auto tag {m_stack.top()};
  if (tag == tags.at(Tags::Uuid)) {
    m_renderer = name;
  } else if (tag == tags.at(Tags::Volume)) {
    m_volume = name.toInt();
  } else if (tag == tags.at(Tags::PlayMode)) {
    m_playMode = name.toInt();
  } else if (tag == tags.at(Tags::RemainingTime)) {
    m_remainingTime = !name.isEmpty() && name.at(0) == '1';
  } else if (tag == tags.at(Tags::Left)) {
    m_rect.setLeft(name.toInt());
  } else if (tag == tags.at(Tags::Top)) {
    m_rect.setTop(name.toInt());
  } else if (tag == tags.at(Tags::Width)) {
    m_rect.setWidth(name.toInt());
  } else if (tag == tags.at(Tags::Height)) {
    m_rect.setHeight(name.toInt());
  } else if (tag == tags.at(Tags::IconSize)) {
    m_iconSize = name.toInt();
    if (m_iconSize < 16) {
      m_iconSize = 16;
    } else if (m_iconSize > 128) {
      m_iconSize = 128;
    }
  } else if (tag == tags.at(Tags::States)) {
    m_windowStates = static_cast<Qt::WindowStates>(name.toInt());
  } else if (tag == tags.at(Tags::Language)) {
    m_language = name;
  } else if (tag == tags.at(Tags::Status)) {
    m_status = name.toUInt(nullptr, 16);
  }
  return true;
}

void CSession::save(QString fileName)
{
  if (m_renderer.isEmpty()) {
    return;
  }
  if (fileName.isEmpty()) {
    fileName = standardFilePath();
  }

  if (fileName.isEmpty()) {
    return;
  }

  QFile file {fileName};
  if (!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
    return;
  }
  QXmlStreamWriter stream {&file};
  stream.setAutoFormatting(true);
  stream.writeStartDocument();

  stream.writeStartElement("session");
  stream.writeAttribute("version", "1.0.0");

  stream.writeStartElement("renderer");

  stream.writeStartElement(tags.at(Tags::Volume));
  stream.writeCharacters(QString::number(m_volume));
  stream.writeEndElement();

  stream.writeStartElement(tags.at(Tags::PlayMode));
  stream.writeCharacters(QString::number(m_playMode));
  stream.writeEndElement();

  stream.writeStartElement(tags.at(Tags::RemainingTime));
  stream.writeCharacters(QString::number(m_remainingTime));
  stream.writeEndElement();

  stream.writeStartElement(tags.at(Tags::Uuid));
  stream.writeCharacters(m_renderer);
  stream.writeEndElement();

  stream.writeEndElement();  // Renderer

  stream.writeStartElement("ui");

  stream.writeStartElement(tags.at(Tags::Left));
  stream.writeCharacters(QString::number(m_rect.left()));
  stream.writeEndElement();

  stream.writeStartElement(tags.at(Tags::Top));
  stream.writeCharacters(QString::number(m_rect.top()));
  stream.writeEndElement();

  stream.writeStartElement(tags.at(Tags::Width));
  stream.writeCharacters(QString::number(m_rect.width()));
  stream.writeEndElement();

  stream.writeStartElement(tags.at(Tags::Height));
  stream.writeCharacters(QString::number(m_rect.height()));
  stream.writeEndElement();

  stream.writeStartElement(tags.at(Tags::States));
  stream.writeCharacters(QString::number(m_windowStates, 16));
  stream.writeEndElement();

  if (m_iconSize != 32) {
    stream.writeStartElement(tags.at(Tags::IconSize));
    stream.writeCharacters(QString::number(m_iconSize));
    stream.writeEndElement();
  }

  stream.writeEndElement();  // ui

  stream.writeStartElement(tags.at(Tags::Language));
  stream.writeCharacters(m_language);
  stream.writeEndElement();

  stream.writeStartElement(tags.at(Tags::Status));
  stream.writeCharacters(QString::number(m_status, 16));
  stream.writeEndElement();

  stream.writeEndElement();  // session
  stream.writeEndDocument();
}

bool CSession::restore(QString fileName)
{
  if (fileName.isEmpty()) {
    fileName = standardFilePath();
  }

  if (fileName.isEmpty()) {
    return false;
  }

  QFile file {fileName};
  if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
    return false;
  }
  const auto data {file.readAll()};
  return parse(data);
}

void CSession::setGeometry(QWidget* widget) const
{
  if (!m_rect.isValid()) {
    return;
  }
  auto wRect {m_rect};
  const auto sRect {QApplication::desktop()->screenGeometry()};
  const auto [dx, dy] {[&]() -> std::pair<int, int>
                       {
                         if (wRect.right() > sRect.right()) {
                           return {sRect.right() - wRect.right(), 0};
                         } else if (wRect.left() < sRect.left()) {
                           return {wRect.left() - sRect.left(), 0};
                         } else if (wRect.bottom() > sRect.bottom()) {
                           return {0, sRect.bottom() - sRect.bottom()};
                         } else if (wRect.top() < sRect.top()) {
                           return {0, wRect.top() - sRect.top()};
                         }
                         return {0, 0};
                       }()};

  if (dx != 0 || dy != 0) {
    wRect.translate(dx, dy);
  }

  widget->setGeometry(wRect);
  widget->setWindowState(m_windowStates);
}
