#ifndef XMLHPLAYLIST_HPP
#define XMLHPLAYLIST_HPP

#include "xmlh.hpp"

class CPlaylist;

/*! Xml handler to read a playlist at format xspf.
 * This handler creates from standard tag of the xspf file the CDidlItems.
 * If QtUPnP extensions tags exists, these are mixed with the previous.
 */
class CXmlHPlaylist : public QtUPnP::CXmlH
{
public:
  explicit CXmlHPlaylist(CPlaylist& playlist);
  ~CXmlHPlaylist() override = default;

  [[nodiscard]] auto startElement(const QString& namespaceURI,
                                  const QString& localName,
                                  const QString&,
                                  const QXmlAttributes&) -> bool override;
  [[nodiscard]] auto characters(const QString& name) -> bool override;

private:
  CPlaylist& m_playlist;
  QString m_name;
};

#endif  // XMLHPLAYLIST_HPP
