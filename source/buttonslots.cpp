#include "aivwidgets/widgethelper.hpp"
#include "avtransport.hpp"
#include "mainwindow.hpp"
#include "renderingcontrol.hpp"
#include "ui_mainwindow.h"

using namespace QtUPnP;

void CMainWindow::on_m_previousStackedWidgetIndex_clicked()
{
  searchAction(false);
  const auto currentIndex {ui->m_stackedWidget->currentIndex()};
  if (currentIndex == Queue || currentIndex == Playing
      || currentIndex == Playlist)
  {
    const auto item {ui->m_folders->top()};
    ui->m_stackedWidget->setCurrentIndex(item.stackedWidgetIndex());
    return;
  }
  if (!ui->m_folders->isEmpty()) {
    const auto item {ui->m_folders->pop()};
    ui->m_stackedWidget->setCurrentIndex(item.stackedWidgetIndex());
    updateContentDirectory(item, true);
    return;
  }

  if (ui->m_stackedWidget->currentIndex() != Home) {
    ui->m_stackedWidget->setCurrentIndex(Home);
  }
}

void CMainWindow::on_m_home_clicked()
{
  ui->m_stackedWidget->setCurrentIndex(Home);
}

void CMainWindow::on_m_currentQueuePlaying_clicked()
{
  auto index {ui->m_stackedWidget->currentIndex() != Queue ? Queue : Playing};
  ui->m_stackedWidget->setCurrentIndex(index);
}

void CMainWindow::on_m_play_clicked()
{
  searchAction(false);
  ui->m_play->setEnabled(false);
  ui->m_play2->setEnabled(false);

  CAVTransport avt {m_cp.get()};
  CTransportInfo info {avt.getTransportInfo(m_renderer)};
  const auto transportState {info.currentTransportState()};
  if (transportState.isEmpty()) {
    return;
  }
  // No error occurs
  auto playing {transportState == "PLAYING"};
  bool success;
  if (playing) {
    QStringList actions = avt.getCurrentTransportActions(m_renderer);
    QStringList::const_iterator begin = actions.cbegin();
    QStringList::const_iterator end = actions.cend();
    QStringList::const_iterator it = std::find(begin, end, "Pause");
    if (it != end) {  // Find Pause invoke the command.
      success = avt.pause(m_renderer);
    } else {  // Pause no found try Stop. If Stop not find assume Pause
              // (probably actions is empty).
      it = std::find(begin, end, "Stop");
      success = it != end ? avt.stop(m_renderer) : avt.pause(m_renderer);
    }

    playing = false;
  } else {
    success = avt.play(m_renderer);
    playing = true;
  }

  if (success) {  // No error occurs
    updatePP(playing);
  }
}

void CMainWindow::on_m_play2_clicked()
{
  on_m_play_clicked();
}

void CMainWindow::on_m_mute_clicked()
{
  searchAction(false);
  ui->m_mute->setEnabled(false);
  ui->m_mute2->setEnabled(false);

  CRenderingControl rc {m_cp.get()};
  const auto mute {!rc.getMute(m_renderer)};
  rc.setMute(m_renderer, mute);
  muteIcon(mute);

  ui->m_mute->setEnabled(true);
  ui->m_mute2->setEnabled(true);
}

void CMainWindow::on_m_mute2_clicked()
{
  on_m_mute_clicked();
}

void CMainWindow::on_m_previous_clicked()
{
  searchAction(false);
  if (!m_cp->playlistName().isEmpty()) {
    CAVTransport {m_cp.get()}.previous(m_renderer);
  } else {
    nextItem(false);
  }
}

void CMainWindow::on_m_previous2_clicked()
{
  on_m_previous_clicked();
}

void CMainWindow::on_m_next_clicked()
{
  searchAction(false);
  if (!m_cp->playlistName().isEmpty()) {
    CAVTransport {m_cp.get()}.next(m_renderer);
  } else {
    nextItem(true);
  }
}

void CMainWindow::on_m_next2_clicked()
{
  on_m_next_clicked();
}

void CMainWindow::on_m_absTime_clicked()
{
  searchAction(false);
  updatePosition();
}

void CMainWindow::on_m_currentPlayingIcon_clicked()
{
  ui->m_stackedWidget->setCurrentIndex(Playing);
}

void CMainWindow::on_m_repeat_clicked()
{
  searchAction(false);
  m_playMode = static_cast<EPlayMode>((m_playMode + 1) % (RepeatOne + 1));
  const auto newPlayMode {playModeString()};
  m_playMode = playMode(newPlayMode);
  CAVTransport {m_cp.get()}.setPlayMode(m_renderer, newPlayMode);
  applyPlayMode();
}

void CMainWindow::on_m_shuffle_clicked()
{
  searchAction(false);
  m_playMode = m_playMode == Shuffle ? Normal : Shuffle;
  const auto newPlayMode {playModeString()};
  CAVTransport {m_cp.get()}.setPlayMode(m_renderer, newPlayMode);
  applyPlayMode();
}

void CMainWindow::on_m_favorite_clicked()
{
  searchAction(false);
  const auto row {ui->m_queue->boldIndex()};
  if (row == -1) {
    return;
  }
  auto* item {
      static_cast<CContentDirectoryBrowserItem*>(ui->m_queue->item(row))};
  if (item == nullptr) {
    return;
  }
  const auto didlItem {item->didlItem()};
  const auto id {didlItem.id()};
  auto index {-1};
  for (auto itPl = m_playlists.begin(), endPl = m_playlists.end();
       itPl != endPl && index == -1;
       ++itPl)
  {
    const auto playlist {itPl.value()};
    if (playlist.isFavorite()) {
      auto items {playlist.items()};
      for (auto itItem = items.cbegin(), endItem = items.cend();
           itItem != endItem && index == -1;
           ++itItem)
      {
        const auto playlistDidlItem {*itItem};
        if (playlistDidlItem.id() == id) {
          index = itItem - items.cbegin();
          items.removeAt(index);
          ui->m_favorite->setIcon(::resIcon("favorite"));
        }
      }
    }
  }

  if (index != -1) {
    return;
  }
  const auto type {CPlaylist::favoritesType(didlItem)};
  auto playlist {favorite(m_playlists, type)};
  if (playlist != nullptr) {
    playlist->addItem(didlItem);
    ui->m_favorite->setIcon(::resIcon("favorite_true"));
  }
}
