#include <QMenu>

#include "aivwidgets/widgethelper.hpp"
#include "avtransport.hpp"
#include "helper.hpp"
#include "mainwindow.hpp"
#include "plugin.hpp"
#include "session.hpp"
#include "ui_mainwindow.h"

using namespace QtUPnP;

void CMainWindow::timerEvent(QTimerEvent* event)
{
  auto updateActions {[this]() -> QList<QAction*>
                      {
                        aboutToShowRenderer();
                        auto* menu {ui->m_renderer->menu()};
                        return menu->actions();
                      }};

  if (m_idDicoveryTimer != event->timerId()) {
    return;
  }
  killTimer(m_idDicoveryTimer);
  if (m_cDiscoverWaits == 0) {  // Launch discovery
    ui->m_home->setIcon(::resIcon("discovery0"));
    m_iconAngle = 0;
    m_cp->avDiscover();
  }

  if (m_cDiscoverWaits == m_cDiscoverMaxWaits - 1) {
    // Not find the last session renderer. Try to find a renderer playing.
    m_renderer.clear();
  }

  auto rendererLastSession {m_renderer};
  if (m_cp->hasRenderer() && ui->m_servers->count() != 0
      && m_cDiscoverWaits < m_cDiscoverMaxWaits)
  {  // Try to find the last session renderer.
    const auto actions {updateActions()};
    if (!m_renderer.isEmpty()) {
      m_renderer.clear();
      std::ranges::for_each(actions,
                            [&](auto* action)
                            {
                              const auto renderer {action->data().toString()};
                              if (renderer == rendererLastSession) {
                                rendererAction(action);
                                m_cDiscoverWaits = m_cDiscoverMaxWaits;
                              }
                            });
    } else {  // Try to find a renderer playing.
      if (!actions.isEmpty()) {
        CAVTransport avt {m_cp.get()};
        std::ranges::for_each(
            actions,
            [&](auto* action)
            {
              const auto renderer {action->data().toString()};
              const auto transportInfo {avt.getTransportInfo(renderer)};
              if (transportInfo.currentTransportState() == "PLAYING") {
                rendererAction(action);
                m_cDiscoverWaits = m_cDiscoverMaxWaits;
              }
            });
      }
    }
  }

  if (m_cDiscoverWaits < m_cDiscoverMaxWaits) {  // Find nothing wait 1s more.
    m_renderer = rendererLastSession;
    m_idDicoveryTimer = startTimer(m_discoverWaitInterval);
    ++m_cDiscoverWaits;
    if (m_iconRotated) {
      rotateIcon();
    }
  } else {
    if (!m_discoveryGuard && m_renderer.isEmpty()) {
      // Find nothing select the first renderer
      const auto uuids {m_cp->renderers()};
      if (!uuids.isEmpty()) {
        const auto actions {updateActions()};
        if (!actions.isEmpty()) {
          rendererAction(actions.first());
        }
      }
    }

    m_discoveryGuard = true;
    m_idDicoveryTimer = startTimer(10000);  // More 10s
    if (m_iconRotated) {
      rotateIcon();
    }
  }
}

void CMainWindow::closeEvent(QCloseEvent* event)
{
  CContentDirectoryBrowser::stopIconUpdateTimer();
  searchAction(false);
  savePlaylists(m_playlists);
  m_positionTimer.stop();
  if (m_cp != nullptr) {
    CSession session {m_renderer,
                      ui->m_volume->value(),
                      m_playMode,
                      ui->m_absTime->isChecked(),
                      normalGeometry(),
                      windowState(),
                      m_iconSize.width(),
                      m_language,
                      m_status.status()};
    session.save();

    const QDir dir {::appDataDirectory()};
    const auto uuids {m_cp->plugins()};
    std::ranges::for_each(
        uuids,
        [&](const auto& uuid)
        {
          auto plugin {m_cp->plugin(uuid)};
          const auto fileName {dir.absoluteFilePath(plugin->name() + ".ids")};
          plugin->saveAuth(fileName);
        });

    m_cp->close();
  }

  QMainWindow::closeEvent(event);
}

void CMainWindow::changeEvent(QEvent* event)
{
  if (event->type() == QEvent::LanguageChange) {
    ui->retranslateUi(this);  // retranslate designer forms
    ui->m_myDevice->retranslateSpecialItems();
    updatePlaylistItemCount();
    addHomePopupMenu();
    if (ui->m_playlistContent->count() != 0) {
      ui->m_provider->setText(ui->m_myDeviceLabel->text());
    } else if (!ui->m_myDevice->selectedItems().isEmpty()) {
      ui->m_provider->setText(ui->m_serverLabel->text());
    }
  }

  QWidget::changeEvent(event);  // remember to call base class implementation
}

void CMainWindow::resizeEvent(QResizeEvent* event)
{
  if (m_startSize.isNull()) {
    m_startSize = event->size();
  }
}
