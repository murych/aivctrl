#ifndef ABOUT_HPP
#define ABOUT_HPP

#include <QDialog>

namespace Ui
{
class CAbout;
}

/*! The about dialog. */
class CAbout : public QDialog
{
  Q_OBJECT

public:
  explicit CAbout(QWidget* parent = nullptr);
  ~CAbout() override;

protected slots:
  void on_m_close_clicked();

private:
  const std::unique_ptr<Ui::CAbout> ui;
};

#endif  // ABOUT_HPP
