#include <QMenu>
#include <QProgressBar>
#include <QProgressDialog>
#include <QTranslator>

#include "about.hpp"
#include "aivwidgets/widgethelper.hpp"
#include "avtransport.hpp"
#include "connectionmanager.hpp"
#include "helper.hpp"
#include "language.hpp"
#include "mainwindow.hpp"
#include "pixmapcache.hpp"
#include "playlistchoice.hpp"
#include "renderingcontrol.hpp"
#include "serverscanner.hpp"
#include "settings.hpp"
#include "ui_mainwindow.h"

using namespace QtUPnP;

void CMainWindow::addRendererPopupMenu()
{
  auto* menu {new QMenu {this}};
  ui->m_renderer->setMenu(menu);
  ui->m_renderer->setPopupMode(QToolButton::InstantPopup);
  connect(menu, &QMenu::aboutToShow, this, &CMainWindow::aboutToShowRenderer);
  connect(menu, &QMenu::triggered, this, &CMainWindow::rendererAction);
}

void CMainWindow::aboutToShowRenderer()
{
  // Devices are sorted by alphabetic order.
  auto compare {[this](const QString& uuid1, const QString& uuid2)
                {
                  const auto name1 {m_cp->device(uuid1).name().toLower()};
                  const auto name2 {m_cp->device(uuid2).name().toLower()};
                  return name1 < name2;
                }};

  // Save the actual device icons.
  auto* menu {ui->m_renderer->menu()};
  std::unordered_map<QString, QIcon> currentActions;
  auto actions {menu->actions()};
  std::ranges::for_each(actions,
                        [&currentActions](auto* action)
                        {
                          const auto renderer {action->data().toString()};
                          currentActions.emplace(renderer, action->icon());
                        });
  menu->clear();

  // For each renderer, create an action.
  auto uuids {m_cp->renderers()};
  std::sort(uuids.begin(), uuids.end(), compare);
  //  for (QString const& uuid : uuids) {
  std::ranges::for_each(
      uuids,
      [&](const auto& uuid)
      {
        const auto device {m_cp->device(uuid)};
        auto* action {new QAction {device.name(), menu}};
        const auto icon {[&]() -> QIcon
                         {
                           if (currentActions.contains(uuid))
                           {  // Restore icon if its saved.
                             return currentActions.at(uuid);
                           }

                           const auto pxmBytes {device.pixmapBytes(
                               m_cp->networkAccessManager(), CDevice::SmResol)};
                           QPixmap pxm;
                           if (pxmBytes.isEmpty()) {
                             pxm.load(::resIconFullPath("device"));
                           } else {
                             pxm.loadFromData(pxmBytes);
                           }

                           return QIcon {pxm};
                         }()};

        action->setIcon(icon);
        action->setData(uuid);
        menu->addAction(action);
      });
}

void CMainWindow::rendererAction(QAction* action)
{
  QString renderer = action->data().toString();
  if (m_renderer == renderer) {
    return;
  }
  // Juste to update variables. Normally eventing must update correctly
  // variable. If protocols is empty the renderer not able to receive or send
  // data. May be it is off or crashed.
  auto protocols {CConnectionManager {m_cp.get()}.getProtocolInfos(renderer)};
  if (protocols.isEmpty()) {
    return;
  }
  if (!m_renderer.isEmpty()) {
    m_cp->unsubscribe(m_renderer);
  }

  m_renderer = renderer;
  const auto device {m_cp->device(renderer)};
  const auto name {device.name()};
  ui->m_rendererName->setText(name);
  ui->m_rendererName->setToolTip(name);
  ui->m_renderer->setIcon(action->icon());
  if (!m_status.hasStatus(UPnPEventsOnly)) {
    m_cp->clearPlaylist();
    const auto var {m_cp->stateVariable(m_renderer, {}, "Volume")};
    auto maximum {static_cast<int>(var.maximum())};
    if (maximum <= 0) {
      maximum = 100;
    }

    ui->m_volume->setMaximum(maximum);
    ui->m_volume2->setMaximum(maximum);
#ifdef Q_OS_MACOS
    ui->m_volume->setTickInterval(maximum / 10);
    ui->m_volume2->setTickInterval(maximum / 10);
#endif
    CRenderingControl rc {m_cp.get()};
    const auto volume {rc.getVolume(m_renderer)};
    updateVolumeSlider(volume, true);

    const auto mute {rc.getMute(m_renderer)};
    muteIcon(mute);

    CAVTransport avt {m_cp.get()};
    const auto transportSettings {avt.getTransportSettings(m_renderer)};
    m_playMode = playMode(transportSettings.playMode());
    applyPlayMode();

    CTransportInfo transportInfo {avt.getTransportInfo(m_renderer)};
    const auto playing {transportInfo.currentTransportState() == "PLAYING"};
    playingIcon(playing);

    // First try to get informations from GetMediaInfo action.
    const auto mediaInfo {avt.getMediaInfo(m_renderer)};
    auto didlMediaInfo {mediaInfo.currentURIDidlItem()};
    if (didlMediaInfo.albumArtURIs().isEmpty())
    {  // No albumArt, try to get informations from GetPositionInfo action.
      auto positionInfo {avt.getPositionInfo(m_renderer)};
      auto didlPositionInfo {positionInfo.didlItem()};
      didlMediaInfo = CDidlItem::mix(didlMediaInfo, didlPositionInfo);
      if (didlMediaInfo.albumArtURIs().isEmpty())
      {  // No albumArt, try to get informations from current queue.
        const auto uri {didlMediaInfo.uri(0)};
        if (!uri.isEmpty()) {
          auto didlItem {ui->m_queue->didlItem(uri)};
          didlMediaInfo = CDidlItem::mix(didlMediaInfo, didlItem);
          if (didlMediaInfo.albumArtURIs().isEmpty())
          {  // No albumArt, try to get informations from current content
             // directory.
            didlItem = ui->m_contentDirectory->didlItem(uri);
            didlMediaInfo = CDidlItem::mix(didlMediaInfo, didlItem);
          }
        }
      }
    }

    updateCurrentPlayling(didlMediaInfo, playing, true);
  }

  m_cp->subscribe(renderer);
  setComRendererIcon();
}

void CMainWindow::addContextualActions(QListWidget* lw, QMenu* menu)
{
  if (lw == nullptr) {
    return;
  }
  auto selectedItems {lw->selectedItems()};
  auto cSelectedItems {selectedItems.size()};
  menu->clear();
  if (lw == ui->m_contentDirectory || lw == ui->m_queue
      || lw == ui->m_playlistContent)
  {
    const auto title {tr("Search")};
    const auto iconName {!ui->m_provider->isReadOnly() ? "search_cancel"
                                                       : "search"};
    auto* action {menu->addAction(::resIcon(iconName), title)};
    action->setData(Search);

    if (cSelectedItems > 0) {
      menu->addSeparator();

      action =
          menu->addAction(::resIcon("replace_queue"), tr("Replace the queue"));
      action->setData(ReplaceQueue);
      action = menu->addAction(::resIcon("add_queue"), tr("Add to the queue"));
      action->setData(AddQueue);
      action->setEnabled(cSelectedItems > 0);

      menu->addSeparator();

      action =
          menu->addAction(::resIcon("add_playlist"), tr("Add to a playlist"));
      action->setData(AddPlaylist);

      menu->addSeparator();

      action =
          menu->addAction(::resIcon("add_favorites"), tr("Add to favorites"));
      action->setData(AddFavorites);

      menu->addSeparator();

      if (lw != ui->m_contentDirectory) {
        menu->addSeparator();
        action = menu->addAction(::resIcon("delete"), tr("Remove"));
        action->setData(RemoveTracks);
      }
    }
    return;
  }

  if (ui->m_myDevice) {
    auto* action {
        menu->addAction(::resIcon("network16"), tr("Look for devices"))};
    action->setData(ScanNetwork);
    if (cSelectedItems != 1) {
      return;
    }
    auto* mdbItem {static_cast<CMyDeviceBrowserItem*>(selectedItems.first())};
    if (mdbItem->isUserPlaylist()) {
      menu->addSeparator();
      action = menu->addAction(::resIcon("rename"), tr("Rename"));
      action->setData(RenamePlaylist);
      action = menu->addAction(::resIcon("delete"), tr("Remove"));
      action->setData(RemovePlaylist);
    }

    if (!mdbItem->isContainer()) {
      const auto playlistName {mdbItem->playlistName()};
      const auto playlist {m_playlists.value(playlistName)};
      if (playlist.items().isEmpty()) {
        return;
      }
      menu->addSeparator();
      action = menu->addAction(::resIcon("search16"), tr("Check playlist"));
      action->setData(CheckPlaylist);
    }
  }
}

void CMainWindow::aboutToShowContextualActions()
{
  auto menu {ui->m_contextualActions->menu()};
  auto index {
      static_cast<EStackedWidgetIndex>(ui->m_stackedWidget->currentIndex())};
  switch (index) {
    case Home:
      addContextualActions(ui->m_myDevice, menu);
      break;

    case ContentDirectory:
      addContextualActions(ui->m_contentDirectory, menu);
      break;

    case Queue:
      addContextualActions(ui->m_queue, menu);
      break;

    case Playlist:
      addContextualActions(ui->m_playlistContent, menu);
      break;

    case Playing:
      addContextualActions(nullptr, menu);
      break;

    default:
      break;
  }
}

void CMainWindow::contextualAction(QAction* action)
{
  if (action == nullptr) {
    return;
  }
  const auto type {static_cast<EActions>(action->data().toInt())};
  if (type == Search) {
    searchAction(ui->m_provider->isReadOnly());
    return;
  }

  searchAction(false);
  auto* widget {ui->m_stackedWidget->currentWidget()};
  if (widget == ui->m_pContentDirectory || widget == ui->m_pQueue
      || widget == ui->m_pPlaylistContent)
  {
    const auto lw {widget->findChild<QListWidget*>()};
    const auto selectedItems {lw->selectedItems()};
    if (!selectedItems.isEmpty()) {
      auto providerText {ui->m_provider->text()};
      switch (type) {
        case ReplaceQueue:
          ui->m_queue->clear();
          [[fallthrough]];
        case AddQueue: {
          QApplication::setOverrideCursor(Qt::WaitCursor);
          const auto didlItems_ {this->didlItems(selectedItems)};
          ui->m_queue->addItems(didlItems_);
          QApplication::restoreOverrideCursor();
          break;
        }

        case AddPlaylist: {
          const auto provider {ui->m_provider->text()};
          const auto didlItems_ {this->didlItems(selectedItems)};
          const auto type_ {CPlaylist::playlistType(didlItems_)};

          auto title {ui->m_myDevice
                          ->containerItem(
                              static_cast<CMyDeviceBrowserItem::EType>(type))
                          ->text()};
          CMyDeviceBrowserItem::removeCount(title);
          CPlaylistChoice plc {
              m_playlists, title, type_, didlItems_.size(), this};
          const auto ret {plc.exec()};
          if (ret == QDialog::Accepted) {
            const auto name {plc.name()};
            const auto newPlaylist {!m_playlists.contains(name)};
            auto playlist {m_playlists.value(name)};
            playlist.setType(type_);
            playlist.addItems(didlItems_);
            if (newPlaylist) {
              auto* item {ui->m_myDevice->addItem(type, name)};
              item = ui->m_myDevice->containerItem(item);
              if (item != nullptr && ui->m_myDevice->isCollapsed(item)) {
                ui->m_myDevice->setCollapsed(item, false);
              }
            }
          }

          ui->m_provider->setText(provider);
          break;
        }

        case AddFavorites: {
          const auto didlItems_ {this->didlItems(selectedItems)};
          const auto type_ {CPlaylist::favoritesType(didlItems_)};
          if (type_ == CPlaylist::Unknown) {
            break;
          }
          std::ranges::for_each(m_playlists,
                                [&type_, &didlItems_](auto& playlist)
                                {
                                  if (playlist.type() == type_) {
                                    playlist.addItems(didlItems_);
                                  }
                                });

          break;
        }

        case RemoveTracks: {
          if (lw == ui->m_playlistContent || lw == ui->m_queue) {
            static_cast<CPlaylistBrowser*>(lw)->delKey();
            ui->m_contextualActions->setEnabled(lw->count() != 0);
          }
          break;
        }

        default:
          break;
      }

      ui->m_provider->setText(providerText);
    }
    return;
  }

  if (widget == ui->m_pHome || widget == ui->m_pDebug) {
    switch (type) {
      case ScanNetwork:
        m_cp->avDiscover();
        break;

      case RemovePlaylist:
        ui->m_myDevice->delKey();
        break;

      case RenamePlaylist:
        ui->m_myDevice->rename();
        break;

      case CheckPlaylist: {
        const auto items {
            ui->m_myDevice->selectedItems()};  // The selected playlist
        if (items.isEmpty()) {
          return;
        }
        ui->m_playlistContent->blockIconUpdate(true);
        auto* item {
            static_cast<CMyDeviceBrowserItem*>(items.first())};  // The playlist
        on_m_myDevice_itemDoubleClicked(item);  // Go to playlist content

        const auto playlistName {item->playlistName()};
        auto playlist {m_playlists.value(playlistName)};
        const auto didlItems {playlist.items()};
        const auto cDidlItems {didlItems.size()};
        if (cDidlItems == 0) {
          return;
        }
        CContentDirectory cd {m_cp.get()};
        QList<int> indices;  // Invalid items indices.
        indices.reserve(cDidlItems);

        // Initialize the progress dialog.
        QProgressDialog progress {
            playlistName, tr("Abort"), 0, cDidlItems, this};
        removeWindowContextHelpButton(&progress);
        progress.setWindowTitle(tr("Check playlist..."));
        progress.setWindowModality(Qt::WindowModal);

        for (int iItem = 0; iItem < cDidlItems; ++iItem) {
          progress.setValue(iItem);
          const auto color {cd.isValidItem(didlItems.at(iItem)) ? Qt::darkGreen
                                                                : Qt::red};
          if (!cd.isValidItem(didlItems.at(iItem)))  // Check validity.
          {  // Store invalid item index.
            indices.push_back(iItem);
          }

          auto* iItemItem {ui->m_playlistContent->item(iItem)};
          if (iItemItem != nullptr) {
            iItemItem->setForeground(color);
            ui->m_playlistContent->scrollToItem(iItemItem);
          }

          if (progress.wasCanceled()) {
            indices.clear();
            break;
          }
        }

        if (!indices.isEmpty()) {  // Try to update item from title.
          QMap<QString, bool> servers;
          CServerScanner scanner {
              m_cp.get(),
              didlItems.at(indices.first()).type(),
              &progress};  // Initialize the forlder scanner.

          auto cIndices {indices.size()};
          auto cUpdated {0};

          // Initialize progress dialog for update.
          progress.setWindowTitle(tr("Update playlist..."));
          auto* pb {progress.findChild<QProgressBar*>()};
          pb->setMaximum(cIndices);

          const auto root {QStringLiteral("0")};
          auto criteria {QStringLiteral("dc:title contains \"%1\"")};
          for (int index = 0; index < cIndices; ++index) {
            progress.setValue(index);
            auto didlItem {didlItems.at(indices.at(index))};
            const auto serverUUID {m_cp->deviceUUID(
                didlItem.uri(0), CDevice::MediaServer)};  // Get the server from
                                                          // uri ip address.
            if (m_status.hasStatus(UseSearchForCheckPlaylist)
                && !servers.contains(serverUUID))
            {
              const auto searchCaps {cd.getSearchCaps(serverUUID)};
              const auto dcTitle {
                  std::find(searchCaps.cbegin(), searchCaps.cend(), "dc:title")
                  != searchCaps.end()};
              servers.insert(serverUUID, dcTitle);
            }

            const auto newDidlItem {
                [&]() -> CDidlItem
                {
                  const auto dcTitle {servers.value(serverUUID)};
                  if (dcTitle) {
                    const auto titleCriteria {
                        criteria.replace("%1", didlItem.title())};
                    const auto reply {
                        cd.search(serverUUID, root, titleCriteria)};
                    if (reply.numberReturned() > 0) {
                      return reply.items().first();
                    }
                  } else {
                    return scanner.scan(
                        serverUUID, didlItem.title());  // Launch server scan
                  }
                  return CDidlItem {};
                }()};

            if (!newDidlItem.isEmpty()) {  // Found same title.
              auto* cdbItem {static_cast<CContentDirectoryBrowserItem*>(
                  ui->m_playlistContent->item(indices.at(index)))};
              cdbItem->setForeground(Qt::darkGreen);
              cdbItem->resetIcon();
              didlItem = newDidlItem;  // Replace item in the playlist.
              ui->m_playlistContent->scrollToItem(cdbItem);
              ++cUpdated;
            }

            const auto text {tr("Updates: ") + QString::number(cUpdated)
                             + tr(" Fails: ")
                             + QString::number(cIndices - cUpdated)};
            progress.setLabelText(text);
            if (progress.wasCanceled()) {
              break;
            }
          }

          progress.setValue(cIndices);
          if (cUpdated) {
            playlist.setChanged();
          }
        } else {
          progress.setValue(cDidlItems);
        }

        ui->m_playlistContent->blockIconUpdate(false);
        ui->m_playlistContent->startIconUpdateTimer();
        break;
      }

      default:
        break;
    }
  }
}

void CMainWindow::addHomePopupMenu()
{
  auto* menu {ui->m_home->menu()};
  if (menu == nullptr) {
    menu = new QMenu {this};
    ui->m_home->setMenu(menu);
    ui->m_home->setPopupMode(QToolButton::InstantPopup);
    connect(menu, &QMenu::triggered, this, &CMainWindow::homeAction);

    auto* action {menu->addAction(::resIcon("settings"), {})};
    action->setData(Settings);

    action = menu->addAction(::resIcon("language"), {});
    action->setData(Language);

    action = menu->addAction(::resIcon("about"), {});
    action->setData(About);
  }

  const auto actions {menu->actions()};
  const std::vector texts {tr("Settings"), tr("Language"), tr("About")};
  for (auto iAction = 0, cActions = actions.size(); iAction < cActions;
       ++iAction)
  {
    actions[iAction]->setText(texts.at(iAction));
  }
}

void CMainWindow::homeAction(QAction* action)
{
  const auto type {static_cast<EActions>(action->data().toInt())};
  switch (type) {
    case Settings: {
      constexpr std::array<EStatus, 5> status {
          ShowNetworkCom,
          UseSearchForCheckPlaylist,
          UPnPEventsOnly,
          UPnPPlaylistDisabled,
          ShowCloudServers,
      };

      std::bitset<CSettings::LastIndex> flags;
      for (size_t index = CSettings::ShowNetworkCom;
           index < CSettings::LastIndex;
           ++index)
      {
        flags[index] = m_status.hasStatus(status[index]);
      }

      auto iconSize {m_iconSize};
      CSettings settings {flags, m_iconSize, this};
      if (settings.exec() == QDialog::Accepted) {
        for (size_t index = CSettings::ShowNetworkCom;
             index < CSettings::LastIndex;
             ++index)
        {
          if (flags.test(index)) {
            m_status.addStatus(status.at(index));
          } else {
            m_status.remStatus(status.at(index));
          }
        }

        ui->m_queue->setDisableUPnPPlaylist(
            m_status.hasStatus(UPnPPlaylistDisabled));
        ui->m_networkProgress->setHidden(!m_status.hasStatus(ShowNetworkCom));
      }

      if (settings.resetState()) {
        resize(m_startSize);
      }

      if (m_iconSize != iconSize) {
        const std::vector<CContentDirectoryBrowser*> cds {
            ui->m_contentDirectory, ui->m_queue, ui->m_playlistContent};
        m_pixmapCache->clear();
        std::ranges::for_each(
            cds,
            [](auto* cd)
            { cd->setIconType(CContentDirectoryBrowserItem::IconNotdefined); });
        cds.at(0)->startIconUpdateTimer();
      }

      break;
    }

    case About: {
      CAbout about {this};
      about.exec();
      break;
    }

    case Language: {
      CLanguage language {this};
      const auto ret {language.exec()};
      if (ret != QDialog::Accepted) {
        break;
      }
      const auto lang {language.shortNameSelected()};
      if (lang != m_language) {
        const auto qmFile {language.qmFileSelected()};
        if (m_translator != nullptr) {
          QCoreApplication::removeTranslator(m_translator);
          delete m_translator;
          m_translator = nullptr;
        }

        m_language = lang;
        if (!qmFile.isEmpty()) {
          const auto qmPath {appFileDir() + "/languages/" + qmFile};
          m_translator = new QTranslator;
          m_translator->load(qmPath);
          QCoreApplication::installTranslator(m_translator);
        } else {
          QCoreApplication::removeTranslator(m_translator);
          delete m_translator;
          m_translator = nullptr;
        }
      }

      break;
    }

    default:
      break;
  }
}

void CMainWindow::contextMenuRequested(QPoint const& pos)
{
  auto* lw {static_cast<QListWidget*>(sender())};
  QMenu menu;
  addContextualActions(lw, &menu);

  CContentDirectoryBrowser::stopIconUpdateTimer();
  const auto globalPos {lw->viewport()->mapToGlobal(pos)};
  const auto selectedAction {menu.exec(globalPos)};
  if (selectedAction != nullptr) {
    contextualAction(selectedAction);
  }
}
