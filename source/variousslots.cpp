#include <QAction>

#include "aivwidgets/networkprogress.hpp"
#include "aivwidgets/widgethelper.hpp"
#include "avtransport.hpp"
#include "mainwindow.hpp"
#include "renderingcontrol.hpp"
#include "ui_mainwindow.h"

using namespace QtUPnP;

void CMainWindow::changeFolder(int index)
{
  searchAction(false);
  if (index == 0) {
    on_m_home_clicked();
    return;
  }
  if (ui->m_folders->erase(index)) {
    const auto item {ui->m_folders->top()};
    updateContentDirectory(item, false);
  }
}

void CMainWindow::on_m_stackedWidget_currentChanged(int index)
{
  searchAction(false);
  auto popupMode {QToolButton::DelayedPopup};
  auto enableContextualActions {false};
  switch (index) {
    case Home:
      CContentDirectoryBrowser::stopIconUpdateTimer();
      ui->m_previousStackedWidgetIndex->setEnabled(false);
      ui->m_folders->clearStack();
      ui->m_playlistContent->clear();
      ui->m_provider->clear();
      updatePlaylistItemCount();
      popupMode = QToolButton::InstantPopup;
      enableContextualActions = true;
      break;

    case ContentDirectory:
      ui->m_contentDirectory->startIconUpdateTimer();
      ui->m_previousStackedWidgetIndex->setEnabled(true);
      ui->m_folders->updateText();
      ui->m_playlistContent->clear();
      ui->m_contentDirectory->setFocus();
      enableContextualActions = ui->m_contentDirectory->count() != 0;
      break;

    case Queue:
      ui->m_queue->startIconUpdateTimer();
      ui->m_folders->setText(tr("Queue"));
      ui->m_currentQueuePlaying->setIcon(::resIcon("queue"));
      ui->m_previousStackedWidgetIndex->setEnabled(true);
      ui->m_playlistContent->clear();
      ui->m_queue->setFocus();
      enableContextualActions = ui->m_queue->count() != 0;
      break;

    case Playing:
      ui->m_currentQueuePlaying->setIcon(::resIcon("current_playing"));
      ui->m_previousStackedWidgetIndex->setEnabled(true);
      ui->m_playlistContent->clear();
      enableContextualActions = false;
      break;

    case Playlist:
      ui->m_playlistContent->startIconUpdateTimer();
      ui->m_previousStackedWidgetIndex->setEnabled(true);
      ui->m_playlistContent->setFocus();
      enableContextualActions = ui->m_playlistContent->count() != 0;
      break;
  }

  const auto hidden {index == Playing};
  ui->m_footer->setHidden(hidden);
  ui->m_myDevice->clearSelection();
  ui->m_home->setPopupMode(popupMode);
  ui->m_contextualActions->setEnabled(enableContextualActions);
}

void CMainWindow::updatePosition()
{
  if (ui->m_position->mousePressed()) {
    return;
  }

  static auto relTimeCount {0};
  const auto maxAbsTime {ui->m_position->maxAbsTime()};
  constexpr auto maxAbsCount {2000000000};  // Probably to big :).

  // Get position from GetPositionInfo action.
  ui->m_position->updatePosition(m_cp.get(), m_renderer);
  const auto positionInfo {ui->m_position->positionInfo()};

  const auto trackURI {positionInfo.trackURI()};
  if (trackURI.isEmpty()) {
    return;
  }
  auto relTimeS {ui->m_position->value()};
  auto absTimeS {ui->m_position->maximum()};

  // Update absTime and relTime widgets.
  auto enable {absTimeS >= 0 && absTimeS <= maxAbsTime};
  ui->m_relTime->setEnabled(enable);
  ui->m_absTime->setEnabled(enable);
  if (!enable) {
    relTimeS = absTimeS = 0;
  }

  if (ui->m_absTime->isChecked()) {
    absTimeS -= relTimeS;
  }

  const auto absTime {QTime {0, 0}.addSecs(absTimeS)};
  const auto formatOut {absTime.hour() == 0 ? "mm:ss" : "hh:mm:ss"};
  ui->m_absTime->setText(absTime.toString(formatOut));

  const auto relTime {QTime {0, 0}.addSecs(relTimeS)};
  ui->m_relTime->setText(relTime.toString(formatOut));

  // Update rank widget.
  auto absCount {positionInfo.absCount()};
  auto relCount {0};
  if (absCount > 0 && absCount < maxAbsCount) {
    relCount = positionInfo.relCount();
  }

  // If relCount not defined, get value from queue list widget.
  if (relCount <= 0 || relCount > maxAbsCount) {
    relCount = ui->m_queue->boldIndex() + 1;
    absCount = ui->m_queue->count();
  }

  const auto numbers {QStringLiteral("%1/%2").arg(relCount).arg(absCount)};
  ui->m_rank->setText(numbers);

  // Set bold the current playing item.
  setItemBold(trackURI);

  // The part of code is needed for renderers that:
  //   - Don't handle playlist or playlist disable.
  //   - Have no SetNextAVTransportURI.
  //   - Don't send event stop at the end of the track.
  if (m_relTimeCurrent == relTimeS) {
    const auto device {m_cp->device(m_renderer)};
    if ((ui->m_queue->isUPnPPlaylistDisabled()
         || device.playlistStatus() != CDevice::PlaylistHandler)
        && !device.hasAction(QString {}, "setNextTransportURI"))
    {  // The device has no setNextTransportURI action.
      ++relTimeCount;
      constexpr auto relTimeCountMax {5};
      if (relTimeCount == relTimeCountMax) {
        auto info {CAVTransport {m_cp.get()}.getTransportInfo(m_renderer)};
        if (info.currentTransportState() != "PAUSED_PLAYBACK") {
          nextItem(true);
        }

        m_relTimeCurrent = relTimeCount = 0;
      }
    }
  } else {
    m_relTimeCurrent = relTimeS;
    relTimeCount = 0;
  }
}

void CMainWindow::showDump()
{
  searchAction(false);
  static int index = 0;
  int newIndex;
  if (ui->m_stackedWidget->currentIndex() != Dump) {
    index = ui->m_stackedWidget->currentIndex();
    newIndex = Dump;
  } else {
    newIndex = index;
  }

  ui->m_stackedWidget->setCurrentIndex(newIndex);
}

void CMainWindow::on_m_provider_textChanged(QString const& text)
{
  if (ui->m_provider->isReadOnly() && text.isEmpty()) {
    return;
  }

  auto* listWidget {ui->m_stackedWidget->currentWidget()
                        ->findChild<CContentDirectoryBrowser*>()};
  if (listWidget == nullptr) {
    return;
  }
  const auto cItems {listWidget->count()};
  QList<CDidlItem> items;
  items.reserve(cItems);
  for (auto iItem = 0; iItem < cItems; ++iItem) {
    auto* item {
        static_cast<CContentDirectoryBrowserItem*>(listWidget->item(iItem))};
    items.push_back(item->didlItem());
  }

  listWidget->clear();
  items = CBrowseReply::search(items, text);
  if (!items.isEmpty()) {
    listWidget->addItems(items);
    listWidget->scrollToTop();
  }
}

void CMainWindow::search()
{
  auto* listWidget {ui->m_stackedWidget->currentWidget()
                        ->findChild<CContentDirectoryBrowser*>()};
  if (listWidget == nullptr) {
    return;
  }
  const auto actions {ui->m_contextualActions->actions()};
  const auto iter {std::ranges::find_if(
      actions,
      [&](QAction* action) { return action->data().toInt() == Search; })};
  if (iter != std::ranges::end(actions)) {
    contextualAction(*iter);
  }
}

void CMainWindow::networkComStarted(CDevice::EType type)
{
  auto* progress {networkProgress(type)};
  if (progress != nullptr) {
    progress->start();
  }
}

void CMainWindow::networkComEnded(CDevice::EType type)
{
  auto* progress {this->networkProgress(type)};
  if (progress != nullptr) {
    progress->stop();
  }
}
