#include "about.hpp"

#include "aivwidgets/widgethelper.hpp"
#include "ui_about.h"

CAbout::CAbout(QWidget* parent)
    : QDialog {parent}
    , ui {std::make_unique<Ui::CAbout>()}
{
  removeWindowContextHelpButton(this);
  ui->setupUi(this);
}

CAbout::~CAbout() = default;

void CAbout::on_m_close_clicked()
{
  accept();
}
