#include "avtransport.hpp"
#include "mainwindow.hpp"
#include "renderingcontrol.hpp"
#include "ui_mainwindow.h"

using namespace QtUPnP;

void CMainWindow::on_m_volume_valueChanged(int volume)
{
  if (!ui->m_volume->signalsBlocked()) {
    searchAction(false);
    CRenderingControl {m_cp.get()}.setVolume(m_renderer, volume);
  }
}

void CMainWindow::on_m_volume2_valueChanged(int volume)
{
  if (!ui->m_volume->signalsBlocked()) {
    searchAction(false);
    CRenderingControl {m_cp.get()}.setVolume(m_renderer, volume);
  }
}

void CMainWindow::on_m_position_valueChanged(int position)
{
  if (ui->m_position->signalsBlocked()) {
    return;
  }
  searchAction(false);

  auto restart {false};
  if (m_positionTimer.isActive()) {
    restart = true;
    m_positionTimer.stop();
  }

  const auto time {QTime {0, 0}.addSecs(position)};
  const auto timePosition {time.toString("hh:mm:ss")};
  CAVTransport {m_cp.get()}.seek(m_renderer, timePosition);
  if (restart) {
    startPositionTimer();
  }
}

void CMainWindow::on_m_volume_actionTriggered(int action)
{
  searchAction(false);
  static_cast<CSlider*>(sender())->jumpToMousePosition(action);
}

void CMainWindow::on_m_volume2_actionTriggered(int action)
{
  searchAction(false);
  static_cast<CSlider*>(sender())->jumpToMousePosition(action);
}

void CMainWindow::on_m_position_actionTriggered(int action)
{
  auto* slider {static_cast<CSlider*>(sender())};
  searchAction(false);
  slider->jumpToMousePosition(action);
}
