#include "playlistchoice.hpp"

#include "aivwidgets/widgethelper.hpp"
#include "helper.hpp"
#include "ui_playlistchoice.h"

CPlaylistChoice::CPlaylistChoice(const QMap<QString, CPlaylist>& playlists,
                                 const QString& title,
                                 CPlaylist::EType type,
                                 int cItems,
                                 QWidget* parent)
    : QDialog {parent}
    , ui {std::make_unique<Ui::CPlaylistChoice>()}
    , m_playlists {playlists}
{
  removeWindowContextHelpButton(this);
  ui->setupUi(this);
  setTransparentBackGround(ui->m_name);
  setTransparentBackGround(ui->m_names);

  const auto text {tr("Playlist name for %1 items").arg(cItems)};
  ui->m_label->setText(text);

  setWindowTitle(title);

  const QIcon icon {::resIcon("playlist")};
  for (auto it = playlists.cbegin(), end = playlists.cend(); it != end; ++it) {
    const auto playlist {it.value()};
    if (playlist.type() == type) {
      new QListWidgetItem {icon, it.key(), ui->m_names};
    }
  }
}

CPlaylistChoice::~CPlaylistChoice() = default;

QString CPlaylistChoice::name() const
{
  return ui->m_name->text();
}

void CPlaylistChoice::on_m_names_itemClicked(QListWidgetItem* item)
{
  ui->m_name->setText(item->text());
}

void CPlaylistChoice::on_m_names_itemDoubleClicked(QListWidgetItem* item)
{
  ui->m_name->setText(item->text());
  accept();
}

void CPlaylistChoice::on_m_ok_clicked()
{
  accept();
}

void CPlaylistChoice::on_m_cancel_clicked()
{
  reject();
}
