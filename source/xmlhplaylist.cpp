#include <QTime>

#include "xmlhplaylist.hpp"

#include "didlitem.hpp"
#include "playlist.hpp"
#include "xmlhdidllite.hpp"

using namespace QtUPnP;

CXmlHPlaylist::CXmlHPlaylist(CPlaylist& playlist)
    : CXmlH {}
    , m_playlist {playlist}
{
}

bool CXmlHPlaylist::startElement(const QString& namespaceURI,
                                 const QString& localName,
                                 const QString& qName,
                                 const QXmlAttributes& atts)
{
  CXmlH::startElement(namespaceURI, localName, qName, atts);
  if (localName == "track") {
    CDidlItem item;
    item.insert("item", CDidlElem());
    m_playlist.items() << item;
  }

  return true;
}

bool CXmlHPlaylist::characters(const QString& name)
{
  if (!m_stack.isEmpty() && !name.trimmed().isEmpty()) {
    const auto s {CDidlItem::percentDecoding(name)};
    const auto tag {m_stack.top()};
    if (tag == "title") {
      const auto tagParent {this->tagParent()};
      if (tagParent == "playlist") {
        m_name = s;
      } else {
        CDidlElem elem;
        elem.setValue(s);
        m_playlist.items().last().insert("dc:title", elem);
      }
    } else if (tag == "location") {
      auto item {m_playlist.items().last()};
      if (!item.elems().contains("res")) {
        CDidlElem elem;
        elem.setValue(s);
        item.insert("res", elem);
      } else {
        CDidlElem elem {m_playlist.items().last().elems().value("res")};
        elem.setValue(s);
        item.replace("res", elem);
      }
    } else if (tag == "album") {
      CDidlElem elem;
      elem.setValue(s);
      m_playlist.items().last().insert("upnp:album", elem);
    } else if (tag == "duration") {
      auto item {m_playlist.items().last()};
      auto duration {QTime {0, 0}.addMSecs(s.toInt()).toString("hh:mm:ss.zzz")};
      if (!item.elems().contains("res")) {
        CDidlElem elem;
        elem.addProp("duration", duration);
        item.insert("res", elem);
      } else {
        CDidlElem elem {m_playlist.items().last().elems().value("res")};
        elem.addProp("duration", duration);
        item.replace("res", elem);
      }
    } else if (tag == "image") {
      CDidlElem elem;
      elem.setValue(s);
      m_playlist.items().last().insert("upnp:albumArtURI", elem);
    } else if (tag == "aivctrl:nrTracks") {
      m_playlist.items().reserve(s.toInt());
    } else if (tag == "aivctrl:playlistType") {
      m_playlist.setType(static_cast<CPlaylist::EType>(s.toInt()));
    } else if (tag == "aivctrl:upnpID") {
      CDidlElem elem {m_playlist.items().last().elems().value("item")};
      elem.addProp("item", s);
      m_playlist.items().last().replace("item", elem);
    } else if (tag == "meta") {
      auto item {m_playlist.items().last()};
      CXmlHDidlLite didLite;
      item = CDidlItem::mix(didLite.firstItem(s), item);
    }
  }

  return true;
}
