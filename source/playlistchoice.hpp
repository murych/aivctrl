#ifndef PLAYLISTCHOICE_HPP
#define PLAYLISTCHOICE_HPP

#include <QDialog>

#include "playlist.hpp"

namespace Ui
{
class CPlaylistChoice;
}

class QListWidgetItem;

/*! Dialog box to choose an existing playlist or create a new playlist. */
class CPlaylistChoice : public QDialog
{
  Q_OBJECT

public:
  explicit CPlaylistChoice(const QMap<QString, CPlaylist>& playlists,
                           const QString& title,
                           CPlaylist::EType type,
                           int cItems,
                           QWidget* parent = nullptr);
  ~CPlaylistChoice() override;

  [[nodiscard]] auto name() const -> QString;

protected slots:
  void on_m_names_itemClicked(QListWidgetItem* item);
  void on_m_names_itemDoubleClicked(QListWidgetItem* item);
  void on_m_ok_clicked();
  void on_m_cancel_clicked();

private:
  const std::unique_ptr<Ui::CPlaylistChoice> ui;
  const QMap<QString, CPlaylist>& m_playlists;
};

#endif  // PLAYLISTCHOICE_HPP
