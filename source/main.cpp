#include <QApplication>

#include "mainwindow.hpp"

int main(int argc, char* argv[])
{
  QApplication a {argc, argv};
  CMainWindow w;
  const QIcon icon {":/icons/aivctrl48.ico"};
  w.setWindowIcon(icon);
  w.show();
  return QApplication::exec();
}
