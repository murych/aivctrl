#include <QDesktopWidget>
#include <QMenu>
#include <QShortcut>

#include "mainwindow.hpp"

#include "aivwidgets/widgethelper.hpp"
#include "dump.hpp"
#include "helper.hpp"
#include "pixmapcache.hpp"
#include "settings.hpp"
#include "ui_mainwindow.h"
#include "xmlh.hpp"

using namespace QtUPnP;

CMainWindow::CMainWindow(QWidget* parent)
    : QMainWindow {parent}
    , ui {std::make_unique<Ui::CMainWindow>()}
    , m_cp {std::make_unique<CControlPoint>()}
    , m_pixmapCache {std::make_unique<CPixmapCache>()}
{
  ui->setupUi(this);
  connect(
      m_cp.get(), &CControlPoint::eventReady, this, &CMainWindow::eventReady);
  connect(m_cp.get(), &CControlPoint::upnpError, this, &CMainWindow::upnpError);
  connect(m_cp.get(),
          &CControlPoint::networkError,
          this,
          &CMainWindow::networkError);
  connect(m_cp.get(), &CControlPoint::newDevice, this, &CMainWindow::newDevice);
  connect(
      m_cp.get(), &CControlPoint::lostDevice, this, &CMainWindow::lostDevice);

  connect(ui->m_folders,
          &CFolderBrowser::indexSelected,
          this,
          &CMainWindow::changeFolder);
  connect(
      &m_positionTimer, &QTimer::timeout, this, &CMainWindow::updatePosition);

  connect(ui->m_myDevice,
          &CMyDeviceBrowser::newPlaylist,
          this,
          &CMainWindow::newPlaylist);
  connect(ui->m_myDevice,
          &CMyDeviceBrowser::renamePlaylist,
          this,
          &CMainWindow::renamePlaylist);
  connect(ui->m_myDevice,
          &CMyDeviceBrowser::removePlaylist,
          this,
          &CMainWindow::removePlaylist);

  connect(
      ui->m_queue, &CPlaylistBrowser::rowsMoved, this, &CMainWindow::rowsMoved);
  connect(
      ui->m_queue, &CPlaylistBrowser::removeIDs, this, &CMainWindow::removeIDs);

  connect(ui->m_playlistContent,
          &CPlaylistBrowser::rowsMoved,
          this,
          &CMainWindow::rowsMoved);
  connect(ui->m_playlistContent,
          &CPlaylistBrowser::removeIDs,
          this,
          &CMainWindow::removeIDs);

  auto ks {QKeySequence {Qt::CTRL + Qt::Key_F11}};
  connect(new QShortcut {ks, this},
          &QShortcut::activated,
          this,
          &CMainWindow::showDump);

  connect(new QShortcut(QKeySequence::Find, this),
          &QShortcut::activated,
          this,
          &CMainWindow::search);
  connect(new QShortcut(QKeySequence::MoveToPreviousChar, this),
          &QShortcut::activated,
          this,
          &CMainWindow::on_m_previous_clicked);
  connect(new QShortcut(QKeySequence::MoveToNextChar, this),
          &QShortcut::activated,
          this,
          &CMainWindow::on_m_next_clicked);

  connect(CDump::dumpObject(),
          &CDump::dumpReady,
          ui->m_dump,
          &QPlainTextEdit::insertPlainText);

  connect(m_cp.get(),
          &CControlPoint::networkComStarted,
          this,
          &CMainWindow::networkComStarted);
  connect(m_cp.get(),
          &CControlPoint::networkComEnded,
          this,
          &CMainWindow::networkComEnded);

  initWidgets();
  ::removeDumpError();
  if (m_status.hasStatus(CreateDumpErrorFile)) {
    CXmlH::setDumpErrorFileName(errorFilePath());
  }

  ui->m_myDevice->createDefaultPlaylists();
  applyLastSession();
  CSettings::setIconSize(this, m_iconSize);
  addRendererPopupMenu();
  addHomePopupMenu();
  ui->m_stackedWidget->setCurrentIndex(Home);
  ui->m_provider->setText(tr("Start look for servers and renderers"));
  loadPlugins();
  m_idDicoveryTimer = startTimer(500);
}

CMainWindow::~CMainWindow() = default;

void CMainWindow::initWidgets()
{
  ui->m_folders->push(Home);

  QStringList defIcons = contentDirectoryIcons();
  CContentDirectoryBrowser* cds[] = {
      ui->m_contentDirectory, ui->m_queue, ui->m_playlistContent};
  for (CContentDirectoryBrowser* cd : cds) {
    cd->setPixmapCache(m_pixmapCache.get());
    cd->setControlPoint(m_cp.get());
    cd->setDefaultIcons(defIcons);
    cd->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(cd,
            &QWidget::customContextMenuRequested,
            this,
            &CMainWindow::contextMenuRequested);
  }

  ui->m_myDevice->setContextMenuPolicy(Qt::CustomContextMenu);
  connect(ui->m_myDevice,
          &QWidget::customContextMenuRequested,
          this,
          &CMainWindow::contextMenuRequested);

  setTransparentBackGround(ui->m_folders);
  setTransparentBackGround(ui->m_provider);
  setTransparentBackGround(ui->m_rendererName);
  setTransparentBackGround(ui->m_title);
  setTransparentBackGround(ui->m_trackMetadata);
  setTransparentBackGround(ui->m_cover);

  ui->m_folders->setReadOnly(true);
  ui->m_provider->setReadOnly(true);
  ui->m_rendererName->setReadOnly(true);
  ui->m_title->setReadOnly(true);

  defIcons = serverIcons();
  ui->m_servers->setDefaultIcons(defIcons);
  QRect desktopRect = QApplication::desktop()->screenGeometry();
  QRect mainWindowRect = geometry();
  if (mainWindowRect.height() > desktopRect.height()) {
    mainWindowRect.setHeight(desktopRect.height());
    ui->m_cover->setMaximumHeight(128);
  }

  if (mainWindowRect.width() > desktopRect.width()) {
    mainWindowRect.setWidth(desktopRect.width());
    ui->m_cover->setMaximumHeight(128);
  }

  ui->m_cover->setDefaultPixmapName("audio_file_big");
  m_positionTimer.setInterval(m_positionTimerInterval);

  QMenu* menu = new QMenu(this);
  ui->m_contextualActions->setMenu(menu);
  connect(menu,
          &QMenu::aboutToShow,
          this,
          &CMainWindow::aboutToShowContextualActions);
  connect(menu, &QMenu::triggered, this, &CMainWindow::contextualAction);

#ifdef Q_OS_MACOS
  ui->m_volume->setTickPosition(QSlider::TicksBothSides);
  ui->m_volume2->setTickPosition(QSlider::TicksBothSides);
  ui->m_position->setTickPosition(QSlider::TicksBothSides);
#endif
}
