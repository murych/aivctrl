#ifndef SETTINGS_HPP
#define SETTINGS_HPP

#include <QDialog>
#include <bitset>

namespace Ui
{
class CSettings;
}

class CSettings : public QDialog
{
  Q_OBJECT
public:
  enum EIndex
  {
    ShowNetworkCom,
    UseSearchForCheckPlaylist,
    UPnPEventsOnly,
    DontUsePlaylists,
    ShowCloudServers,
    LastIndex,
  };

  explicit CSettings(std::bitset<LastIndex> flags,
                     QSize& iconSize,
                     QWidget* parent = nullptr);
  ~CSettings() override;

  auto resetState() const { return m_reset; }

  static void setIconSize(QWidget* w, const QSize& size);

private slots:
  void on_m_reset_clicked();
  void on_m_ok_clicked();
  void on_m_cancel_clicked();
  void on_m_iconSize_valueChanged(int value);

private:
  std::unique_ptr<Ui::CSettings> ui {nullptr};
  std::bitset<LastIndex> m_flags;
  QSize& m_iconSize;
  QSize m_iconSizeBackup;
  bool m_reset {false};
};

#endif  // SETTINGS_HPP
