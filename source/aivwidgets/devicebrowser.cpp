#include "devicebrowser.hpp"

#include "QShortcut"
#include "widgethelper.hpp"

using namespace QtUPnP;

CDeviceBrowserItem::CDeviceBrowserItem(QListWidget* parent, int type)
    : QListWidgetItem {parent, type}
{
}

CDeviceBrowserItem::CDeviceBrowserItem(CDevice const& device,
                                       QStringList const& defaultIcons,
                                       QListWidget* parent,
                                       int type)
    : QListWidgetItem {parent, type}
    , m_device {device}
{
  setText(device.name());
  const auto bytes {device.pixmapBytes(nullptr, CDevice::SmResol)};
  QPixmap pxm;
  if (!bytes.isEmpty() && pxm.loadFromData(bytes) && !pxm.isNull()) {
    setIcon(QIcon {pxm});
  }

  const auto deviceType {device.type()};
  if (pxm.isNull() && deviceType < defaultIcons.size()) {
    pxm.load(::resIconFullPath(defaultIcons.at(deviceType)));
    setIcon(QIcon {pxm});
  }

  updateTooltip();
}

CDeviceBrowserItem::CDeviceBrowserItem(CDeviceBrowserItem const& other)
    : QListWidgetItem {other}
    , m_device {other.m_device}
{
}

void CDeviceBrowserItem::updateTooltip()
{
  const auto string {m_device.name().isEmpty() ? QObject::tr("Unknown")
                                               : m_device.name()};

  auto tooltip {TOOLTIPFIELD1("Title", string)};
  tooltip += TOOLTIPFIELD("Url", m_device.url().toString());
  tooltip += TOOLTIPFIELD("Model name", m_device.modelName());
  tooltip += TOOLTIPFIELD("Model number", m_device.modelNumber());
  tooltip += TOOLTIPFIELD("Model url", m_device.modelURL());
  tooltip += TOOLTIPFIELD("Description", m_device.modelDesc());
  tooltip += TOOLTIPFIELD("Presentation", m_device.presentationURL());
  tooltip += TOOLTIPFIELD("Manufacturer", m_device.manufacturer());
  tooltip += TOOLTIPFIELD("Manufacturer url", m_device.manufacturerURL());
  tooltip += TOOLTIPFIELD("Device type", m_device.deviceType());
  const auto version {QStringLiteral("%1.%2")
                          .arg(m_device.majorVersion())
                          .arg(m_device.minorVersion())};
  tooltip += TOOLTIPFIELD("Version", version);

  if (m_device.type() == CDevice::MediaRenderer) {
    const auto status {m_device.playlistStatus()};
    const auto manage {[status]() -> QString
                       {
                         switch (status) {
                           case CDevice::NoPlaylistHandler:
                             return "no";

                           case CDevice::PlaylistHandler:
                             return "yes";

                           default:
                             return "unknown";
                         }
                       }()};

    tooltip += TOOLTIPFIELD("Manage playlists", manage);
  }

  setToolTip(tooltip);
}

CDeviceBrowser::CDeviceBrowser(QWidget* parent)
    : CListWidgetBase {parent}
{
  setTransparentBackGround(this);
  connect(new QShortcut {QKeySequence {Qt::Key_Escape}, this},
          &QShortcut::activated,
          this,
          &CListWidgetBase::clearSelection);
}

void CDeviceBrowser::addItem(CDevice const& device)
{
  new CDeviceBrowserItem {device, m_defaultIcons, this};
}

void CDeviceBrowser::delItem(QString const& uuid)
{
  for (int row = 0, cRows = count(); row < cRows; ++row) {
    auto* item {static_cast<CDeviceBrowserItem*>(this->item(row))};
    const auto device {item->device()};
    if (device.uuid() == uuid) {
      delete item;
      break;
    }
  }
}

QIcon CDeviceBrowser::icon(QString const& uuid)
{
  for (int row = 0, cRows = count(); row < cRows; ++row) {
    auto* item {static_cast<CDeviceBrowserItem*>(this->item(row))};
    const auto device {item->device()};
    if (device.uuid() == uuid) {
      return item->icon();
    }
  }

  return {};
}
