#include "listwidgetbase.hpp"

#include "widgethelper.hpp"

CListWidgetBase::CListWidgetBase(QWidget* parent)
    : QListWidget {parent}
{
  setTransparentBackGround(this);
#ifdef Q_OS_LINUX
  setItemMouseOverColor(this);
#endif
  connect(this,
          &CListWidgetBase::itemClicked,
          this,
          &CListWidgetBase::saveSelected);
  connect(this,
          &CListWidgetBase::itemDoubleClicked,
          this,
          &CListWidgetBase::saveSelected);
}

void CListWidgetBase::saveSelected(QListWidgetItem* item)
{
  m_selectedRow = row(item);
}

int CListWidgetBase::boldIndex() const
{
  auto index {-1};
  for (int iItem = 0, end = count(); iItem < end; ++iItem) {
    auto* item {this->item(iItem)};
    const auto font {item->font()};
    if (font.bold()) {
      index = iItem;
      break;
    }
  }

  return index;
}
