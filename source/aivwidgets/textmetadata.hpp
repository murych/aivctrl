#ifndef TEXTMETADATA_HPP
#define TEXTMETADATA_HPP

#include <QTextEdit>

/*! \brief This class provides functionalities to show a paragraphe from a
 * QStringList.
 *
 * One line for QStringList QString. A html text is sent at the QTextEdit.
 */
class CTextMetaData : public QTextEdit
{
  Q_OBJECT
public:
  /*! Default constructor. */
  explicit CTextMetaData(QWidget* parent = nullptr);
  ~CTextMetaData() override = default;

  /*! Sets the texts. */
  void setText(const QStringList& texts);

  /*! Truncates texts.The texts are truncated at the widget size. */
  [[nodiscard]] auto truncateText(const QString& text) -> QString;

  /*! Clears all texts. */
  void clear();

  /*! Truncates texts.The texts are truncated at the widget size. */
  [[nodiscard]] static auto truncateText(const QString& text, QWidget* widget)
      -> QString;

protected:
  /*! Retruncates texts at the new size */
  void resizeEvent(QResizeEvent* event) override;

  /*! Updates the QTextEdit. */
  void updateText();

protected:
  QStringList m_texts;  //!< The texts. One for each line.
};

#endif  // TEXTMETADATA_HPP
