#ifndef SLIDER_HPP
#define SLIDER_HPP

#include <QSlider>

/*! \brief This class changes the page up and page down QSlider mechanism.
 *
 * When page up or page down is clicked, the value goes to the position of the
 * cursor. By default the value is incremented or decremented by pageStep.
 */
class CSlider : public QSlider
{
public:
  /*! Default constructor. */
  explicit CSlider(QWidget* parent = nullptr);
  ~CSlider() override = default;

  /*! Sets the QSlider value at the cursor position. */
  void jumpToMousePosition(int action);

  /*! Returns the flag m_mousePressed. */
  [[nodiscard]] auto mousePressed() const { return m_mousePressed; }

protected:
  /*! Sets the flag m_mousePressed at true. */
  void mousePressEvent(QMouseEvent* event) override;

  /*! Sets the flag m_mousePressed at false. */
  void mouseReleaseEvent(QMouseEvent* event) override;

protected:
  bool m_mousePressed {false};  //!< Flag m_mousePressed.
};

#endif  // SLIDER_HPP
