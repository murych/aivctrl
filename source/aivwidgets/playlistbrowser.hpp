#ifndef PLAYLISTBROWSER_HPP
#define PLAYLISTBROWSER_HPP

#include "contentdirectorybrowser.hpp"

namespace QtUPnP
{
class CControlPoint;
}

/*! \brief Playlist content browser. */
class CPlaylistBrowser : public CContentDirectoryBrowser
{
  Q_OBJECT

public:
  /*! Default constructor. */
  explicit CPlaylistBrowser(QWidget* parent = nullptr);
  ~CPlaylistBrowser() override = default;

  /*! Invokes SetAVTransportURI action. */
  void setAVTransportURI(QtUPnP::CControlPoint* cp,
                         const QString& renderer,
                         int itemRow);

  /*! Invokes SetNextAVTransportURI action. */
  void setNextAVTransportURI(QtUPnP::CControlPoint* cp,
                             const QString& renderer);

  /*! Returns the playlist name. */
  [[nodiscard]] auto name() const { return m_name; }

  /*! Sets the playlist name. */
  void setName(const QString& name) { m_name = name; }

  /*! Disable/Enable the use of UPnP playlist. */
  void setDisableUPnPPlaylist(bool disable) { m_disableUPnPPlaylist = disable; }

  /*! Returns true the use of UPnP playlist is disable. */
  [[nodiscard]] auto isUPnPPlaylistDisabled() const
  {
    return m_disableUPnPPlaylist;
  }

  /*! Enable/Disable HTTP conversion for HTTPS protocol for streaming. */
  void setDirectStreamHTTPS(bool set) { m_directStreamHTTPS = set; }

public slots:
  /*! Removes an item. */
  void delKey();

signals:
  /*! An item has been removed. */
  void removeIDs(const QString& name, const QStringList& ids);

  /*! Items has been moved (drag and drop). */
  void rowsMoved(const QModelIndex& parent,
                 int start,
                 int end,
                 const QModelIndex& destination,
                 int row);

  /*! Items are about to moved. */
  void rowsAboutToBeMoved(const QModelIndex& parent,
                          int start,
                          int end,
                          const QModelIndex& destination,
                          int row);

private:
  void replaceNoHttpScheme(QtUPnP::CDidlItem& didlItem);

protected:
  QString m_name;  //!< Playlist name.
  bool m_disableUPnPPlaylist {false};  //!< Do not user playlists.
  bool m_directStreamHTTPS {
      false};  //!< The renderer can handle HTTPS protocol.
};

#endif  // PLAYLISTBROWSER_HPP
