#ifndef MYDEVICEBROWSER_HPP
#define MYDEVICEBROWSER_HPP

#include <QListWidget>

/*! \brief It is the item of CMyDeviceBrowser. */
class CMyDeviceBrowserItem : public QListWidgetItem
{
public:
  /*! Item type. */
  enum EType
  {
    AudioContainer,  //!< Audio container item.
    AudioFavorite,  //!< Audio favorite item.
    AudioPlaylist,  //!< Audio user playlist item.
    ImageContainer,  //!< Image container item.
    ImageFavorite,  //!< Image favorite item.
    ImagePlaylist,  //!< Image user playlist item.
    VideoContainer,  //!< Video container item.
    VideoFavorite,  //!< Video favorite item.
    VideoPlaylist,  //!< Video user playlist item.
  };

  /*! Default constructor. */
  CMyDeviceBrowserItem(const QIcon& icon, const QString& name, EType type);
  ~CMyDeviceBrowserItem() override = default;

  /*! Returns the type of the item. */
  [[nodiscard]] auto type() const { return m_type; }

  /*! Sets the type of the item. */
  void setType(EType type) { m_type = type; }

  /*! Returns true if the item is a container. */
  [[nodiscard]] auto isContainer() const -> bool;

  /*! Returns true if the item is a user playlist. */
  [[nodiscard]] auto isUserPlaylist() const -> bool;

  /*! Returns true if the item is a favorite. */
  [[nodiscard]] auto isFavorite() const -> bool;

  /*! Returns the default user playlist name from the item type. */
  [[nodiscard]] auto defaultNewPlaylistName() const
  {
    return defaultNewPlaylistName(m_type);
  }

  /*! Returns favorite name from the item type. */
  [[nodiscard]] auto favoritesPlaylistName() const
  {
    return favoritesPlaylistName(m_type);
  }

  /*! Returns the playlist name. */
  [[nodiscard]] auto playlistName() const -> QString;

  /*! Set count at te item. */
  void setCount(int count);

  /*! Lessthan operator for sorting. */
  [[nodiscard]] auto operator<(const QListWidgetItem& other) const
      -> bool override;

  /*! Returns the default user playlist name from the type. */
  [[nodiscard]] static auto defaultNewPlaylistName(int type) -> QString;

  /*! Returns the default favorite name from the type. */
  [[nodiscard]] static auto favoritesPlaylistName(int type) -> QString;

  /*! Remove count at the item title. */
  static auto removeCount(QString& text) -> int;

  /*! Returns true if the item is a container. */
  [[nodiscard]] static auto isContainer(EType type) -> bool;

protected:
  EType m_type {AudioContainer};  //!< The item type.
};

/*! \brief This class provides functionalities to manage playlists names.
 *
 * CMyDeviceBrowser contains automatically:
 * \li An item for playlist container for each type (audio, image, video).
 * \li An item for favorites for each type.
 * \li A set of user playlists for each type.
 */
class CMyDeviceBrowser : public QListWidget
{
  Q_OBJECT
public:
  /*! Default constructor. */
  explicit CMyDeviceBrowser(QWidget* parent = nullptr);
  ~CMyDeviceBrowser() override = default;

  /*! Retranslates favorites and containers titles. */
  void retranslateSpecialItems();

  /*! Creates default playlists. */
  void createDefaultPlaylists();

  /*! Collapses or expands container items. */
  void setCollapsed(CMyDeviceBrowserItem::EType type, bool collapse);

  /*! Collapses or expands container items. */
  void setCollapsed(CMyDeviceBrowserItem* item, bool collapse);

  /*! Toggles collapse or expand container items. */
  bool collapseExpand(CMyDeviceBrowserItem* item);

  /*! Toggle collapse or expand container items. */
  bool isCollapsed(CMyDeviceBrowserItem* item);

  /*! Manages container double click. */
  void containerDoubleClicked(CMyDeviceBrowserItem* item);

  /*! Returns the item from its row. */
  [[nodiscard]] auto* item(int row)
  {
    return static_cast<CMyDeviceBrowserItem*>(this->QListWidget::item(row));
  }

  /*! Update the name of the playlist. For playlists with same name,
   * CMyDeviceBrowser add an index. E.g. if the playlist named "new playlist"
   * exists and if the another playlist is created with the same name,
   * CMyDeviceBrowser rename the new playlist in "new playlist-2" and maybe "new
   * playlist-3", "new playlist-4"...
   */
  QString updateName(QListWidgetItem* item);

  /*! Adds a new playlist name. */
  auto addItem(int type, QString const& name) -> CMyDeviceBrowserItem*;

  /*! Returns a container item from an item. */
  [[nodiscard]] auto containerItem(CMyDeviceBrowserItem* item)
      -> CMyDeviceBrowserItem*;

  /*! Returns a container item from a type. */
  [[nodiscard]] auto containerItem(CMyDeviceBrowserItem::EType type)
      -> CMyDeviceBrowserItem*;

  /*! Returns the first item for a CMyDeviceBrowserItem::EType. */
  [[nodiscard]] auto item(CMyDeviceBrowserItem::EType type)
      -> CMyDeviceBrowserItem*;

  /*! Sets count at an item. */
  void setCount(CMyDeviceBrowserItem::EType type, int count);

  /*! Set count at a playlist name. */
  void setCount(const QString& name, int count);

  /*! Updates item count. */
  void updateContainerItemCount();

public slots:
  /*! Removes a playlist. */
  void delKey();

  /*! Renames a playlist. */
  void rename();

protected slots:
  /*! Update the name of the playlist. */
  void commitData(QWidget* editor) override;

  /*! Generate item double click for key Return. */
  void keyReturn();

signals:
  /*! A new playlist has been created. */
  void newPlaylist(const QString& name, int type);

  /*! A playlist has been renamed. */
  void renamePlaylist(const QString& name, const QString& newName);

  /*! A playlist has been memoved. */
  void removePlaylist(const QString& name);

private:
  QColor m_rootColor {0x0, 0x59, 0xc7, 30};  //!< Container item color.
};

#endif  // MYDEVICEBROWSER_HPP
