#ifndef WIDGETHELPER_HPP
#define WIDGETHELPER_HPP

#include <QIcon>

class QWidget;
class QListWidget;
class QDialog;
class QStringList;
class QString;

/*! \brief This file contains functions to refractorize the code. */

/*! Calls tooltipFied as being the first field of the tooltip. */
#define TOOLTIPFIELD1(title, value) \
  tooltipField(QObject::tr(title), value, true)

/*! Calls tooltipFied as being the non first field of the tooltip. */
#define TOOLTIPFIELD(title, value) \
  tooltipField(QObject::tr(title), value, false)

/*! Sets backgroung widget transparent. */
void setTransparentBackGround(QWidget* widget);

/*! Sets item mouse over color. */
void setItemMouseOverColor(QListWidget* widget);

/*! Removes the windows context help button. */
void removeWindowContextHelpButton(QDialog* dialog);

/*! Returns the field value.
 * \param title: The title.
 * \param value: The QString value.
 * \param first: True if this field is the first field.
 * \return The field value.
 */
[[nodiscard]] auto tooltipField(const QString& title,
                                const QString& value,
                                bool first = false) -> QString;

/*! Same as above with unsigned integer value. */
[[nodiscard]] auto tooltipField(const QString& title,
                                unsigned value,
                                bool first = false) -> QString;

/*! Same as above with unsigned long long integer value. */
[[nodiscard]] auto tooltipField(const QString& title,
                                unsigned long long value,
                                bool first = false) -> QString;

/*! Same as above with double value. */
[[nodiscard]] auto tooltipField(const QString& title,
                                double value,
                                bool first = false) -> QString;

/*! Returns the icon full path. */
[[nodiscard]] auto resIconFullPath(const QString& name) -> QString;

/*! Returns the icon from name. */
[[nodiscard]] auto resIcon(const QString& name) -> QIcon;

/*! Sets the icon directory. By default ":/icons for a resource. */
void setIconDirRes(const QString& dir);

/*! Sets the icon suffix. By default ".png". */
void setIconSuffixRes(const QString& suffix);

/*! Updates name.
 * From a list of existing names, this function returns a new name.
 * For example, if name is "New playlist", and if "New playlist" exists in
 * existingNames, The function returns "New playlist-2". If "New playlist-2"
 * exists, the return is "New playlist-3". and so on.
 */
[[nodiscard]] auto updateName(const QString& name,
                              const QStringList& existingNames) -> QString;

#endif  // WIDGETHELPER_HPP
