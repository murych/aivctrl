#include "position.hpp"

#include "_deps/qtupnp-src/source/helper.hpp"
#include "avtransport.hpp"

CPosition::CPosition(QWidget* parent)
    : CSlider {parent}
{
}

void CPosition::updatePosition(QtUPnP::CControlPoint* cp,
                               const QString& renderer)
{
  QtUPnP::CAVTransport avt {cp};
  m_positionInfo = avt.getPositionInfo(renderer);
  if (m_positionInfo.trackURI().isEmpty()) {
    return;
  }
  const auto absTime {
      m_positionInfo.trackDuration()};  // Try track duration member
  auto absTimeMS {
      static_cast<int>(QtUPnP::timeToS(absTime))};  // Convert to milliseconds.
  if (absTimeMS <= 0 || absTimeMS > m_maxAbsTime)
  {  // Bad abs time. Some renderer defines absTime to string and return
     // 2147483647. Try duration from res elems.
    auto didlItem {m_positionInfo.didlItem()};
    const auto duration {
        didlItem.duration(-1)};  // Get first elem with property duration.
    absTimeMS = static_cast<int>(QtUPnP::timeToS(duration));
    if (absTimeMS <= 0 || absTimeMS > m_maxAbsTime) {
      setEnabled(false);
      return;
    }
  }

  setEnabled(true);
  blockSignals(true);
  setMaximum(absTimeMS);
#ifdef Q_OS_MACOS
  setTickInterval(absTimeMS / 10);
#endif
  const auto relTime {m_positionInfo.relTime()};
  auto relTimeMS {static_cast<int>(QtUPnP::timeToS(relTime))};
  if (relTimeMS < 0) {  // Bad relTime.
    relTimeMS = 0;
  }

  if (relTimeMS > absTimeMS) {  // Bad real time.
    relTimeMS = absTimeMS;
  }

  setValue(relTimeMS);
  blockSignals(false);
}
