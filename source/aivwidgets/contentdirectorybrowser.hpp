#ifndef CONTENTDIRECTORYDBROWSER_HPP
#define CONTENTDIRECTORYDBROWSER_HPP

#include <QTimer>

#include "contentdirectory.hpp"
#include "listwidgetbase.hpp"

namespace QtUPnP
{
class CControlPoint;
class CPixmapCache;
}  // namespace QtUPnP

/*! brief The CContentDirectoryBrowserItem class provides an item for use with
 * the CContentDirectoryBrowser. This class adds CDidlItem at QListWidgetItem
 * used to display server content directory items, playlists items, ... It also
 * manages the icon for each item.
 */
class CContentDirectoryBrowserItem : public QListWidgetItem
{
public:
  /*! The icon type. */
  enum EIconType
  {
    IconNotdefined,  //!< Not define.
    IConStandard,  //!< The standard icon is used.
    IconServer  //!< The icon is provided by the server.
  };

  /*! Default constructor. */
  explicit CContentDirectoryBrowserItem(QListWidget* parent = nullptr,
                                        int type = Type);

  /*! The constructor with the CDidlItem. */
  explicit CContentDirectoryBrowserItem(const QtUPnP::CDidlItem& didlItem,
                                        QListWidget* parent = nullptr,
                                        int type = Type);

  /*! The copy constructor. */
  CContentDirectoryBrowserItem(const CContentDirectoryBrowserItem& other);

  /*! Set the icon at undefined. */
  void resetIcon() { m_iconType = IconNotdefined; }

  /*! Returns the type of icon. */
  [[nodiscard]] auto iconType() const { return m_iconType; }

  /*! Sets the type of icon. */
  void setIconType(EIconType type) { m_iconType = type; }

  /*! Returns the CDidlItem. */
  [[nodiscard]] auto didlItem() const { return m_didlItem; }

  /*! Udates the icon from the cache or from the server. */
  void updateIcon(QtUPnP::CControlPoint* cp,
                  QtUPnP::CPixmapCache* cache,
                  int iconIndex,
                  const QStringList& defaultPixmaps,
                  const QSize& iconSize);

  /*! Update the tooltip. */
  void updateTooltip();

  /*! Returns the standrad icon from the type of CDidlItem. */
  [[nodiscard]] static auto standardIcon(const QtUPnP::CDidlItem& didlItem,
                                         const QStringList& defaultPixmaps)
      -> QPixmap;

  /*! Changes the timeout to get icon from the server. */
  static void setAlbumArtURITimeout(int timeout)
  {
    CContentDirectoryBrowserItem::m_albumArtURITimeout = timeout;
  }

  /*! Returns the icon timeout. */
  [[nodiscard]] static auto albumArtURITimeout()
  {
    return CContentDirectoryBrowserItem::m_albumArtURITimeout;
  }

protected:
  QtUPnP::CDidlItem m_didlItem;  //!< The item returned by browse action.
  EIconType m_iconType {IconNotdefined};  //!< The tye of icon.
  static int m_albumArtURITimeout;  //!< The timeout to retreave the icon from
                                    //!< the server.
};

/*! \brief The CContentDirectoryBrowser class provides a
 * CContentDirectoryBrowserItem list widget.
 *
 * This class is used to display server content directory, playlists, ...
 * The icons are updated with thumbnails provided the cache or by the server.
 * Because update icons can be very slow, a timer is responsible for updating
 * icons. Using this method, it is possible to stop at any time the update. For
 * exemple when the widget is scrolled.
 */
class CContentDirectoryBrowser : public CListWidgetBase
{
  Q_OBJECT

public:
  /*! Default constructor. */
  explicit CContentDirectoryBrowser(QWidget* parent = nullptr);
  ~CContentDirectoryBrowser() override = default;

  /*! Sets the pixmap cache. */
  void setPixmapCache(QtUPnP::CPixmapCache* pixmapCache)
  {
    m_pixmapCache = pixmapCache;
  }

  /*! Sets the sort criteria.
   * Without criteria, CContentDirectoryBrowser show items in the order defined
   * by the server. It is possible to sort items from a value or a property. The
   * criteria is similar at UPnP criteria. To sort items by value: \li Sort by
   * titles, the criteria is "dc:title". \li Sort bt creators, the criteria is
   * "dc:creator". \li Sort by genre, the criteria is "upnp:genre". \li Sort by
   * album, the criteria is "upnp:album". \li Sort by date, the criteria is
   * "dc:date".
   *
   * To sort items by a property:
   * \li Sort by size, the criteria is "res@size".
   * \li Sort by duration, the criteria is "res@duration".
   * \li Sort by bitrate, the criteria is "res@bitrate".
   */
  void setSortCriteria(QString const& criteria) { m_sortCriteria = criteria; }

  /*! Defines the direction of sorting. Must be CBrowseReply::ascending or
   * CBrowseReply::descending. */
  void setSortDir(QtUPnP::CBrowseReply::ESortDir dir) { m_sortDir = dir; }

  /*! Returns the pixmap cache. */
  [[nodiscard]] auto pixmapCache() const { return m_pixmapCache; }

  /*! Returns the actual sort criteria. */
  [[nodiscard]] auto sortCriteria() const { return m_sortCriteria; }

  /*! Returns the actual sort direction. */
  [[nodiscard]] auto sortDir() const { return m_sortDir; }

  /*! Adds CContentDirectoryBrowserItem from the list of CDidlItems. */
  [[nodiscard]] auto addItems(const QList<QtUPnP::CDidlItem>& didlItems) -> int;

  /*! Adds from an id and a server. The arguments are identical at browse
   * action. */
  [[nodiscard]] auto addItems(
      const QString& server,
      const QString& id,
      QtUPnP::CContentDirectory::EBrowseType type =
          QtUPnP::CContentDirectory::BrowseDirectChildren,
      QString filter = {},
      int startingIndex = 0,
      int requestedCount = 0,
      const QString& sortCriteria = {}) -> int;

  /*! Starts the timer to update icons. */
  void startIconUpdateTimer();

  /*! Sets bold an item with the uri equal at the argument. */
  void setBold(const QString& uri);

  /*! Sets the control point. */
  void setControlPoint(QtUPnP::CControlPoint* cp) { m_cp = cp; }

  /*! Return the index previous or next. */
  [[nodiscard]] auto nextIndex(bool forward, bool repeat, bool shuffle) -> int;

  /*! Returns the CDidlItem with the uri equal at the argument. */
  [[nodiscard]] auto didlItem(const QString& uri) const -> QtUPnP::CDidlItem;

  /*! Resets the number of items selected. */
  void resetSelectedCount() { m_cSelected = 0; }

  /*! Decrement the number of items selected. */
  void decSelectedCount() { --m_cSelected; }

  /*! Increment the number of items selected. */
  void incSelectedCount() { ++m_cSelected; }

  /*! Returns the number of items selected. */
  [[nodiscard]] auto selectedCount() const { return m_cSelected; }

  /*! Block or release the update of the icons. */
  void blockIconUpdate(bool block) { m_blockIconUpdate = block; }

  /*! Returns the status blocked or not of the icons. */
  [[nodiscard]] auto iconUpdateBlocked() const { return m_blockIconUpdate; }

  /*! Sets icon type at all items. */
  void setIconType(CContentDirectoryBrowserItem::EIconType type);

  /*! Stops the update of the icons. */
  static void stopIconUpdateTimer();

protected slots:
  /*! The scroll bar position has changed. The update icon timer starts. */
  void scrollBarValueChanged(int);

  /*! Updates the icons. Called by the icon timer. */
  void updateIcon();

  /*! The item will be played. Called by return key. */
  void startStream();

protected:
  /*! The widget size has changed. The update icon timer starts. */
  void resizeEvent(QResizeEvent*) override;

  QtUPnP::CPixmapCache* m_pixmapCache {nullptr};  //!< The icon cache.
  QtUPnP::CControlPoint* m_cp {nullptr};  //!< The control point.
  QString m_sortCriteria;  //!< The sort criteria.
  QtUPnP::CBrowseReply::ESortDir m_sortDir;  //!< The sort direction.
  int m_cSelected {0};  //!< The current number of item selected.
  bool m_blockIconUpdate {false};  //!< Block or release the update of icons.

  static QTimer
      m_iconUpdateTimer;  //!< One timer for all CContentDirectoryBrowser
  static int const m_iconUpdateThresholdTime;  //!< Start interval(500ms)
  static int const m_iconUpdateRepeatTime;  //!< After start interval, reduce
                                            //!< interval at 10ms.
  static CContentDirectoryBrowser*
      m_directoryBrowser;  //!< CContentDirectoryBrowser for the current timer.
};

#endif  // CONTENTDIRECTORYDBROWSER_HPP
