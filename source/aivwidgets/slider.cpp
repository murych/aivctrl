#include <QDebug>
#include <QMouseEvent>
#include <QProxyStyle>
#include <QStyleOptionSlider>

#include "slider.hpp"

CSlider::CSlider(QWidget* parent)
    : QSlider {parent}
{
  setTracking(false);
}

void CSlider::mousePressEvent(QMouseEvent* event)
{
  QSlider::mousePressEvent(event);
  m_mousePressed = true;
  // Very important because the slider can enter in an infinit loop.
  setRepeatAction(QAbstractSlider::SliderNoAction);
}

void CSlider::mouseReleaseEvent(QMouseEvent* event)
{
  QSlider::mouseReleaseEvent(event);
  m_mousePressed = false;
  // Very important because the slider can enter in an infinit loop.
  setRepeatAction(QAbstractSlider::SliderNoAction);
}

void CSlider::jumpToMousePosition(int action)
{
  if (signalsBlocked()
      && !(action == QAbstractSlider::SliderPageStepAdd
           || action == QAbstractSlider::SliderPageStepSub))
  {
    return;
  }

  QStyleOptionSlider opt;
  opt.initFrom(this);
  auto sr {style()->subControlRect(
      QStyle::CC_Slider, &opt, QStyle::SC_SliderHandle, this)};
  auto localPos {mapFromGlobal(QCursor::pos())};
  auto normalizedPosition {
      [&]() -> float
      {
        if (orientation() == Qt::Vertical) {
          auto halfHandleHeight {(0.5f * sr.height()) + 0.5f};
          auto height {this->height()};
          auto adaptedPosY {height - localPos.y()};
          if (adaptedPosY < halfHandleHeight) {
            adaptedPosY = halfHandleHeight;
          }

          if (adaptedPosY > height - halfHandleHeight) {
            adaptedPosY = height - halfHandleHeight;
          }

          auto newHeight {(height - halfHandleHeight) - halfHandleHeight};
          return (adaptedPosY - halfHandleHeight) / newHeight;
        }

        auto halfHandleWidth {(0.5f * sr.width()) + 0.5f};
        auto adaptedPosX {localPos.x()};
        if (adaptedPosX < halfHandleWidth) {
          adaptedPosX = halfHandleWidth;
        }

        auto width {this->width()};
        if (adaptedPosX > width - halfHandleWidth) {
          adaptedPosX = width - halfHandleWidth;
        }

        auto newWidth {(width - halfHandleWidth) - halfHandleWidth};
        return (adaptedPosX - halfHandleWidth) / newWidth;
      }()};

  auto newVal {minimum() + ((maximum() - minimum()) * normalizedPosition)};
  if (invertedAppearance()) {
    newVal = maximum() - newVal;
  }

  setValue(newVal);
}
