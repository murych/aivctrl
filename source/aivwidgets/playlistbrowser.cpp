#include <QMessageBox>
#include <QShortcut>

#include "playlistbrowser.hpp"

#include "avtransport.hpp"
#include "plugin.hpp"

using namespace QtUPnP;

CPlaylistBrowser::CPlaylistBrowser(QWidget* parent)
    : CContentDirectoryBrowser {parent}
{
  setDragEnabled(true);
  setDragDropMode(QAbstractItemView::InternalMove);
  connect(model(),
          &QAbstractItemModel::rowsMoved,
          this,
          &CPlaylistBrowser::rowsMoved);
  connect(new QShortcut {QKeySequence {Qt::Key_Delete}, this},
          &QShortcut::activated,
          this,
          &CPlaylistBrowser::delKey);
}

void CPlaylistBrowser::setAVTransportURI(CControlPoint* cp,
                                         QString const& renderer,
                                         int itemRow)
{
  CAVTransport avt {cp};
  avt.stop(renderer);  // Some renderers return wrong values if the function is
                       // not called in mode "PLAYING".
  const auto device {cp->device(renderer)};

  auto cdItem {static_cast<CContentDirectoryBrowserItem*>(item(itemRow))};
  auto didlItem {cdItem->didlItem()};
  auto type {didlItem.type()};
  if (!m_disableUPnPPlaylist && type != CDidlItem::AudioBroadcast
      && type != CDidlItem::VideoBroadcast
      && device.playlistStatus() == CDevice::PlaylistHandler)
  {
    auto playlistName {CHTTPServer::formatUUID(renderer)};
    auto seekTo {0};
    if (cp->playlistName() != playlistName) {
      QList<CDidlItem::TPlaylistElem> playlistElems;
      auto cItems {count()};
      playlistElems.reserve(cItems);

      for (int iItem = 0; iItem < cItems; ++iItem) {
        auto itemIItem {
            static_cast<CContentDirectoryBrowserItem*>(item(iItem))};
        auto didlItem_ {itemIItem->didlItem()};
        replaceNoHttpScheme(didlItem_);
        playlistElems.push_back(CDidlItem::TPlaylistElem(didlItem_, 0));
        if (iItem == itemRow) {
          seekTo = iItem + 1;
        }
      }

      avt.setAVTransportURI(
          renderer, playlistName, playlistElems);  // For a playlist.
      avt.waitForAVTransportURI(renderer, seekTo);
    } else {
      seekTo = itemRow + 1;
    }

    avt.seek(renderer, seekTo);
  } else {
    replaceNoHttpScheme(didlItem);
    avt.setAVTransportURI(renderer, didlItem);  // For a single file.
  }

  auto info {avt.getTransportInfo(renderer)};
  const auto state {info.currentTransportState()};
  if (state != "PLAYING" && state != "TRANSITIONING") {
    avt.play(renderer);
  }
}

void CPlaylistBrowser::setNextAVTransportURI(CControlPoint* cp,
                                             const QString& renderer)
{
  const auto device {cp->device(renderer)};
  if (!device.hasAction(QString {}, "SetNextAVTransportURI")) {
    return;
  }
  auto cItems {count()};
  auto iItem {0};
  for (; iItem < cItems; ++iItem) {
    auto item {this->item(iItem)};
    auto font {item->font()};
    if (font.bold()) {
      break;
    }
  }

  if (iItem < cItems - 1) {
    auto cdItem {static_cast<CContentDirectoryBrowserItem*>(item(iItem + 1))};
    auto didlItem {cdItem->didlItem()};
    replaceNoHttpScheme(didlItem);
    CAVTransport {cp}.setNextAVTransportURI(renderer, didlItem);
  }
}

void CPlaylistBrowser::delKey()
{
  auto items {selectedItems()};
  if (items.isEmpty()) {
    return;
  }

  constexpr auto maxTitles {10};
  constexpr auto maxTitleLen {60};

  clearSelection();
  QStringList ids, titles;
  ids.reserve(items.size());
  titles.reserve(maxTitles);

  std::ranges::for_each(
      items,
      [&](auto* item)
      {
        auto cdbItem {static_cast<CContentDirectoryBrowserItem*>(item)};
        const auto didlItem {cdbItem->didlItem()};
        ids.append(didlItem.itemID());
        if (titles.size() >= maxTitles) {
          return;
        }
        auto title {didlItem.title()};
        if (title.length() > maxTitleLen) {
          title.truncate(maxTitleLen);
          title.append("...");
        }
        titles.append(didlItem.title());
      });

  auto text {QStringLiteral("<ul type=\"1\">")};
  std::ranges::for_each(titles,
                        [&text](const auto& title)
                        { text.append("<li>" + title + "</li>"); });

  if (ids.size() > maxTitles) {
    text.append("<li>" + QString::number(ids.size() - maxTitleLen)
                + QObject::tr(" more...") + "</li>");
  }

  text.append("</ul>");
  auto btn {QMessageBox::question(this, QObject::tr("Confirm removing"), text)};
  if (btn == QMessageBox::Yes) {
    qDeleteAll(items);
    emit removeIDs(m_name, ids);
  }
}

void CPlaylistBrowser::replaceNoHttpScheme(CDidlItem& didlItem)
{
  if (m_directStreamHTTPS) {
    return;
  }
  QUrl url {didlItem.uri(0)};
  if (url.scheme()
      == "http")  // Assume http protocol is handled by the renderer.
  {  // Probably https
    return;
  }
  auto pluginUUID {didlItem.value("aivctrl:plugin").value()};
  if (pluginUUID.isEmpty()) {
    return;
  }
  CPlugin* plugin = m_cp->plugin(pluginUUID);
  if (plugin == nullptr) {
    return;
  }
  pluginUUID = CHTTPServer::formatUUID(pluginUUID);
  const auto uri {m_cp->serverListenAddress() + "plugin" + '/' + pluginUUID
                  + '/' + didlItem.uri(0)};
  auto didlElem {didlItem.value("res")};
  didlElem.setValue(uri);
  didlItem.replace("res", didlElem);
}
