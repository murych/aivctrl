#include <QMouseEvent>

#include "folderbrowser.hpp"

#include "listwidgetbase.hpp"

struct SFolderItemData : public QSharedData
{
  SFolderItemData() = default;
  SFolderItemData(int stackedWidgetIndex,
                  QString const& name,
                  QString const& parentID = QString(),
                  QString const& id = QString(),
                  CListWidgetBase* listWidget = nullptr);
  SFolderItemData(SFolderItemData const& other);

  int m_stackedWidgetIndex {0};
  QString m_name;
  QString m_parentID;
  QString m_id;
  CListWidgetBase* m_listWidget {nullptr};
  int m_row {-1};
};

SFolderItemData::SFolderItemData(SFolderItemData const& other)
    : QSharedData {other}
    , m_stackedWidgetIndex {other.m_stackedWidgetIndex}
    , m_name {other.m_name}
    , m_parentID {other.m_parentID}
    , m_id {other.m_id}
    , m_listWidget {other.m_listWidget}
    , m_row {other.m_row}
{
}

SFolderItemData::SFolderItemData(int stackedWidgetIndex,
                                 QString const& name,
                                 QString const& parentID,
                                 QString const& id,
                                 CListWidgetBase* listWidget)
    : QSharedData {}
    , m_stackedWidgetIndex {stackedWidgetIndex}
    , m_name {name}
    , m_parentID {parentID}
    , m_id {id}
    , m_listWidget {listWidget}
{
  if (m_listWidget != nullptr) {
    m_row = m_listWidget->selectedRow();
  }
}

CFolderItem::CFolderItem()
    : m_d {new SFolderItemData}
{
}

CFolderItem::CFolderItem(int index,
                         const QString& name,
                         const QString& parentID,
                         const QString& id,
                         CListWidgetBase* listWidget)
    : m_d {new SFolderItemData {index, name, parentID, id, listWidget}}
{
}

CFolderItem::CFolderItem(const CFolderItem& rhs)
    : m_d {rhs.m_d}
{
}

CFolderItem& CFolderItem::operator=(const CFolderItem& rhs)
{
  if (this != &rhs) {
    m_d.operator=(rhs.m_d);
  }

  return *this;
}

CFolderItem::~CFolderItem() = default;

void CFolderItem::setStackedWidgetIndex(int index)
{
  m_d->m_stackedWidgetIndex = index;
}

void CFolderItem::setID(QString const& id)
{
  m_d->m_id = id;
}

void CFolderItem::setParentID(QString const& id)
{
  m_d->m_parentID = id;
}

void CFolderItem::setListWidget(CListWidgetBase* listWidget)
{
  m_d->m_listWidget = listWidget;
}

void CFolderItem::setRow(int row)
{
  m_d->m_row = row;
}

void CFolderItem::setName(QString const& name)
{
  m_d->m_name = name;
}

int CFolderItem::stackedWidgetIndex() const
{
  return m_d->m_stackedWidgetIndex;
}

QString CFolderItem::id() const
{
  return m_d->m_id;
}

QString CFolderItem::parentID() const
{
  return m_d->m_parentID;
}

CListWidgetBase* CFolderItem::listWidget() const
{
  return m_d->m_listWidget;
}

int CFolderItem::row() const
{
  return m_d->m_row;
}

QString CFolderItem::name() const
{
  return m_d->m_name;
}

void CFolderItem::setListWidgetCurrentItem() const
{
  if (m_d->m_listWidget == nullptr && m_d->m_row == -1) {
    return;
  }
  auto* item {m_d->m_listWidget->item(m_d->m_row)};
  if (item != nullptr) {
    item->setSelected(true);
    m_d->m_listWidget->scrollToItem(item);
  }
}

CFolderBrowser::CFolderBrowser(QWidget* parent)
    : QLineEdit {parent}
{
}

bool CFolderBrowser::findFolder(int& start, int& end, int pos)
{
  auto find {false};
  const auto text {this->text()};
  const auto tlen {text.length()};
  const auto slen {m_separator.length()};
  if (pos < 0 && pos >= tlen) {
    return false;
  }

  start = end = pos;
  while (start >= 1 && text.mid(start, slen) != m_separator) {
    --start;
  }

  if (text.indexOf(m_separator, start) == start) {
    start += slen;
  }

  while (end < tlen && text.mid(end, slen) != m_separator) {
    ++end;
  }

  return true;
}

void CFolderBrowser::mouseMoveEvent(QMouseEvent* event)
{
  const auto cursorPos {event->pos()};
  const auto pos {cursorPositionAt(cursorPos)};
  auto start {0}, end {0};
  if (findFolder(start, end, pos)) {
    deselect();
    m_selectedFolder = false;
    if (end != text().length()) {
      setSelection(start, end - start);
      m_selectedFolder = true;
    }
  }
}

void CFolderBrowser::mouseReleaseEvent(QMouseEvent* event)
{
  if (!m_selectedFolder) {
    return;
  }
  const auto cursorPos {event->pos()};
  const auto pos {cursorPositionAt(cursorPos)};
  const auto text {this->text().left(pos)};
  auto index {text.count(m_separator) + 2};
  if (index >= m_items.size()) {
    index = 0;
  }

  emit indexSelected(index);
}

void CFolderBrowser::enterEvent(QEvent*)
{
  deselect();
  m_selectedFolder = false;
}

void CFolderBrowser::leaveEvent(QEvent*)
{
  deselect();
  m_selectedFolder = false;
}

void CFolderBrowser::updateText()
{
  QString text;
  std::ranges::for_each(m_items,
                        [&](const auto& item)
                        {
                          const auto name {item.name()};
                          if (!text.isEmpty()) {
                            text += m_separator;
                          }

                          text += name;
                        });

  setText(text);
  setToolTip(text);
}

void CFolderBrowser::push(int index,
                          QString name,
                          QString const& parentID,
                          QString const& id,
                          CListWidgetBase* listWidget)
{
  m_items.push(CFolderItem {index, name, parentID, id, listWidget});
  if (name.startsWith(m_slash)) {
    setText(name.remove(0, 1));
    setText(name);
  } else {
    updateText();
  }
}

CFolderItem CFolderBrowser::pop()
{
  const auto item {m_items.pop()};
  updateText();
  return item;
}

void CFolderBrowser::clearStack()
{
  if (m_items.size() > 1) {
    const auto item {m_items.first()};
    auto* listWidget {item.listWidget()};
    auto row {item.row()};
    if (listWidget != nullptr && row != -1) {
      listWidget->setCurrentRow(row);
    }

    m_items.erase(m_items.begin() + 1, m_items.end());
  }

  QLineEdit::clear();
}

bool CFolderBrowser::erase(int start, int end)
{
  const auto success {!m_items.isEmpty() && start >= 0
                      && start < m_items.size()};
  if (success) {
    const auto itStart {m_items.begin() + start};
    const auto itEnd {end == -1 || end > m_items.size()
                          ? m_items.end()
                          : m_items.begin() + end};
    m_items.erase(itStart, itEnd);
    updateText();
  }

  return success;
}
