#include <QPainter>

#include "cover.hpp"

#include "datacaller.hpp"
#include "widgethelper.hpp"

using namespace QtUPnP;

CCover::CCover(QWidget* parent)
    : QLabel {parent}
{
}

void CCover::setImage(QString const& uri)
{
  auto defName {true};
  if (!uri.isEmpty()) {
    auto pxmBytes {CDataCaller().callData(uri, m_imageTimeout)};
    if (!pxmBytes.isEmpty()) {
      m_pixmap.loadFromData(pxmBytes);
      if (!m_pixmap.isNull()) {
        defName = false;
      }
    }
  }

  if (defName) {
    m_pixmap = QPixmap(::resIconFullPath(m_defaultPixmap));
  }

  update();
}

void CCover::paintEvent(QPaintEvent* event)
{
  if (!m_pixmap.isNull()) {
    const auto wl {width()}, hl {height()};
    const auto wp {m_pixmap.width()}, hp {m_pixmap.height()};
    const auto ws {wl / wp};
    const auto hs {hl / hp};
    const auto pxm {
        ws < hs ? m_pixmap.scaledToWidth(wl, Qt::SmoothTransformation)
                : m_pixmap.scaledToHeight(hl, Qt::SmoothTransformation)};

    QPainter paint {this};
    const auto x {(wl - pxm.width()) / 2};
    const auto y {(hl - pxm.height()) / 2};
    if (!isEnabled()) {
      paint.setOpacity(0.2);
    }
    paint.drawPixmap(x, y, pxm);
  } else {
    QLabel::paintEvent(event);
  }
}
