#ifndef DEVICEBROWSER_HPP
#define DEVICEBROWSER_HPP

#include "device.hpp"
#include "listwidgetbase.hpp"

/*! \brief This class adds CDevice at QListWidgetItem used to display devices.
 * It is used to show servers.
 */
class CDeviceBrowserItem : public QListWidgetItem
{
public:
  /*! Default constructor. */
  explicit CDeviceBrowserItem(QListWidget* parent = nullptr, int type = Type);

  /*! Constructor with device and default icons. */
  explicit CDeviceBrowserItem(QtUPnP::CDevice const& device,
                              QStringList const& defaultIcons,
                              QListWidget* parent = nullptr,
                              int type = Type);

  /*! Copy constructor. */
  CDeviceBrowserItem(CDeviceBrowserItem const& other);

  /*! Sets the device to the item. */
  void setDevice(QtUPnP::CDevice const& device) { m_device = device; }

  /*! Returns the device of the item. */
  [[nodiscard]] auto device() const { return m_device; }

  /*! Update the tooltip. */
  void updateTooltip();

private:
  QtUPnP::CDevice m_device;  //!< The device.
};

/*! \brief The CDeviceBrowser class provides an CDeviceBrowserItem list widget.
 */
class CDeviceBrowser : public CListWidgetBase
{
  Q_OBJECT

public:
  /*! Default constructor. */
  explicit CDeviceBrowser(QWidget* parent = nullptr);
  ~CDeviceBrowser() override = default;

  /*! Adds a device to the list widget. */
  void addItem(const QtUPnP::CDevice& device);

  /*! Deletes a device of the list widget. */
  void delItem(const QString& uuid);

  [[nodiscard]] auto icon(const QString& uuid) -> QIcon;
};

#endif  // DEVICEBROWSER_HPP
