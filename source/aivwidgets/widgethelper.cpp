#include <QDialog>
#include <QDir>
#include <QListWidget>

#include "widgethelper.hpp"

namespace
{
QDir s_iconDirRes {":/icons"};
QString s_iconSuffixRes {".png"};
}  // namespace

void setTransparentBackGround(QWidget* widget)
{
  auto* parent {widget->parentWidget()};
  if (parent == nullptr) {
    return;
  }
  const auto color {parent->palette().color(parent->backgroundRole())};
  auto palette {widget->palette()};
  palette.setColor(QPalette::Base, color);
  widget->setPalette(palette);
}

void setItemMouseOverColor(QListWidget* widget)
{
  auto palette {widget->palette()};
  auto itemSelectedColor {palette.color(QPalette::Highlight)};
  itemSelectedColor.setAlpha(164);
  const auto styleSheet {
      QStringLiteral("QListView::item:hover {background: #%1;}")
          .arg(itemSelectedColor.rgba(), 0, 16)};
  widget->setStyleSheet(styleSheet);
}

void removeWindowContextHelpButton(QDialog* dialog)
{
  auto flags {dialog->windowFlags()};
  flags = flags & (~Qt::WindowContextHelpButtonHint);
  dialog->setWindowFlags(flags);
}

QString tooltipField(const QString& title, const QString& value, bool first)
{
  if (!value.isEmpty()) {
    return first ? QStringLiteral("<b>%1: %2</b>").arg(title, value)
                 : QStringLiteral("<br>%1: %2").arg(title, value);
  }

  return {};
}

QString tooltipField(const QString& title, unsigned value, bool first)
{
  return tooltipField(title, static_cast<unsigned long long>(value), first);
}

QString tooltipField(const QString& title, unsigned long long value, bool first)
{
  if (value != 0) {
    return tooltipField(title, QString::number(value), first);
  }

  return {};
}

QString tooltipField(const QString& title, double value, bool first)
{
  if (static_cast<int>(value) != 0) {
    return tooltipField(title, QString::number(value), first);
  }

  return {};
}

void setIconDirRes(const QString& dir)
{
  s_iconDirRes.setPath(dir);
}

void setIconSuffixRes(const QString& suffix)
{
  s_iconSuffixRes = suffix;
}

QString resIconFullPath(const QString& name)
{
  QFileInfo fi {s_iconDirRes, name};
  if (fi.suffix().isEmpty()) {
    fi.setFile(s_iconDirRes, name + s_iconSuffixRes);
  }

  return fi.absoluteFilePath();
}

QIcon resIcon(const QString& name)
{
  return QIcon {resIconFullPath(name)};
}

QString updateName(const QString& name, const QStringList& existingNames)
{
  const auto name_exists {
      std::find(existingNames.cbegin(), existingNames.cend(), name)
      == existingNames.end()};
  if (name_exists) {
    return name;
  }

  QString result {name};
  QRegularExpression rx {"-([0-9]+)"};
  auto index {name.lastIndexOf(rx)};
  if (index != -1) {
    result.truncate(index);
  }

  auto indexMax {-1};
  std::ranges::for_each(existingNames,
                        [&](const auto& existingName)
                        {
                          QString simplifiedName = existingName;
                          index = existingName.lastIndexOf(rx);
                          if (index != -1) {
                            simplifiedName.truncate(index);
                          }

                          if (simplifiedName != result) {
                            return;
                          }
                          if (indexMax == -1) {
                            indexMax = 1;
                          }

                          auto number {existingName.mid(index)};
                          number.remove('-');
                          auto index_ {number.toInt()};
                          if (index_ > indexMax) {
                            indexMax = index_;
                          }
                        });

  if (indexMax != -1) {
    ++indexMax;
    result += QString("-%1").arg(indexMax);
  }

  return result;
}
