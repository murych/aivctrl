#include <QMessageBox>
#include <QRegularExpression>
#include <QShortcut>

#include "mydevicebrowser.hpp"

#include "widgethelper.hpp"

CMyDeviceBrowserItem::CMyDeviceBrowserItem(const QIcon& icon,
                                           const QString& name,
                                           EType type)
    : QListWidgetItem {icon, name}
    , m_type {type}
{
}

bool CMyDeviceBrowserItem::isContainer() const
{
  return m_type == AudioContainer || m_type == ImageContainer
      || m_type == VideoContainer;
}

bool CMyDeviceBrowserItem::isContainer(EType type)
{
  return type == AudioContainer || type == ImageContainer
      || type == VideoContainer;
}

bool CMyDeviceBrowserItem::isUserPlaylist() const
{
  return m_type == CMyDeviceBrowserItem::AudioPlaylist
      || m_type == CMyDeviceBrowserItem::ImagePlaylist
      || m_type == CMyDeviceBrowserItem::VideoPlaylist;
}

bool CMyDeviceBrowserItem::isFavorite() const
{
  return m_type == CMyDeviceBrowserItem::AudioFavorite
      || m_type == CMyDeviceBrowserItem::ImageFavorite
      || m_type == CMyDeviceBrowserItem::VideoFavorite;
}

bool CMyDeviceBrowserItem::operator<(QListWidgetItem const& other) const
{
  if (m_type < static_cast<const CMyDeviceBrowserItem&>(other).m_type) {
    return true;
  }

  if (m_type == static_cast<const CMyDeviceBrowserItem&>(other).m_type) {
    return text() < other.text();
  }

  return false;
}

QString CMyDeviceBrowserItem::favoritesPlaylistName(int type)
{
  switch (type) {
    case CMyDeviceBrowserItem::AudioFavorite:
      return "Audio favorites";

    case CMyDeviceBrowserItem::ImageFavorite:
      return "Image favorites";

    case CMyDeviceBrowserItem::VideoFavorite:
      return "Video favorites";

    default:
      return {};
  }
}

QString CMyDeviceBrowserItem::defaultNewPlaylistName(int type)
{
  switch (type) {
    case CMyDeviceBrowserItem::AudioContainer:
    case CMyDeviceBrowserItem::AudioFavorite:
    case CMyDeviceBrowserItem::AudioPlaylist:
      return QObject::tr("New audio playlist");

    case CMyDeviceBrowserItem::ImageContainer:
    case CMyDeviceBrowserItem::ImageFavorite:
    case CMyDeviceBrowserItem::ImagePlaylist:
      return QObject::tr("New image playlist");

    default:
      return QObject::tr("New video playlist");
  }
}

QString CMyDeviceBrowserItem::playlistName() const
{
  auto name {favoritesPlaylistName()};
  if (name.isEmpty()) {
    name = this->text();
    removeCount(name);
  }

  return name;
}

int CMyDeviceBrowserItem::removeCount(QString& text)
{
  const QRegularExpression rx {" \\(([0-9]+)\\)"};
  auto index {text.lastIndexOf(rx)};
  if (index == -1) {
    return -1;
  }

  auto count {0};
  auto coef {1};
  for (int k = text.length() - 2; text[k].isNumber(); --k) {
    count += (text.at(k).toLatin1() - '0') * coef;
    coef *= 10;
  }

  text.truncate(index);
  return count;
}

void CMyDeviceBrowserItem::setCount(int count)
{
  auto text {this->text()};
  const auto cItems {removeCount(text)};
  if (count != cItems) {
    text += QStringLiteral(" (%1)").arg(count);
    setText(text);
  }
}

CMyDeviceBrowser::CMyDeviceBrowser(QWidget* parent)
    : QListWidget {parent}
{
  setTransparentBackGround(this);
#ifdef Q_OS_LINUX
  setItemMouseOverColor(this);
#endif
  connect(new QShortcut {QKeySequence {Qt::Key_Delete}, this},
          &QShortcut::activated,
          this,
          &CMyDeviceBrowser::delKey);
  connect(new QShortcut {QKeySequence {Qt::Key_Escape}, this},
          &QShortcut::activated,
          this,
          &CMyDeviceBrowser::clearSelection);
  connect(new QShortcut {QKeySequence {Qt::Key_Return}, this},
          &QShortcut::activated,
          this,
          &CMyDeviceBrowser::keyReturn);
}

void CMyDeviceBrowser::createDefaultPlaylists()
{
  setSortingEnabled(false);
  auto title {QStringLiteral("Audio playlists")};
  auto item {new CMyDeviceBrowserItem {
      ::resIcon("audio"), title, CMyDeviceBrowserItem::AudioContainer}};
  item->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
  item->setBackground(m_rootColor);
  QListWidget::addItem(item);

  title = "Image playlists";
  item = new CMyDeviceBrowserItem(
      ::resIcon("image"), title, CMyDeviceBrowserItem::ImageContainer);
  item->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
  item->setBackground(m_rootColor);
  QListWidget::addItem(item);

  title = "Video playlists";
  item = new CMyDeviceBrowserItem {
      ::resIcon("video"), title, CMyDeviceBrowserItem::VideoContainer};
  item->setTextAlignment(Qt::AlignHCenter | Qt::AlignVCenter);
  item->setBackground(m_rootColor);
  QListWidget::addItem(item);

  QIcon icon {::resIcon("favorites")};
  title = CMyDeviceBrowserItem::favoritesPlaylistName(
      CMyDeviceBrowserItem::AudioFavorite);
  item = new CMyDeviceBrowserItem {
      icon, title, CMyDeviceBrowserItem::AudioFavorite};
  QListWidget::addItem(item);
  emit newPlaylist(title, CMyDeviceBrowserItem::AudioFavorite);

  title = CMyDeviceBrowserItem::favoritesPlaylistName(
      CMyDeviceBrowserItem::ImageFavorite);
  item = new CMyDeviceBrowserItem {
      icon, title, CMyDeviceBrowserItem::ImageFavorite};
  QListWidget::addItem(item);
  emit newPlaylist(title, CMyDeviceBrowserItem::ImageFavorite);

  title = CMyDeviceBrowserItem::favoritesPlaylistName(
      CMyDeviceBrowserItem::VideoFavorite);
  item = new CMyDeviceBrowserItem {
      icon, title, CMyDeviceBrowserItem::VideoFavorite};
  QListWidget::addItem(item);
  emit newPlaylist(title, CMyDeviceBrowserItem::VideoFavorite);
  setSortingEnabled(true);
  sortItems();
}

void CMyDeviceBrowser::retranslateSpecialItems()
{
  for (int iItem = 0, cItems = count(); iItem < cItems; ++iItem) {
    auto item {static_cast<CMyDeviceBrowserItem*>(this->item(iItem))};
    const auto name {[&]() -> QString
                     {
                       switch (item->type()) {
                         case CMyDeviceBrowserItem::AudioContainer:
                           return QObject::tr("Audio playlists");

                         case CMyDeviceBrowserItem::AudioFavorite:
                           return QObject::tr("Audio favorites");

                         case CMyDeviceBrowserItem::ImageContainer:
                           return QObject::tr("Image playlists");

                         case CMyDeviceBrowserItem::ImageFavorite:
                           return QObject::tr("Image favorites");

                         case CMyDeviceBrowserItem::VideoContainer:
                           return QObject::tr("Video playlists");

                         case CMyDeviceBrowserItem::VideoFavorite:
                           return QObject::tr("Video favorites");

                         default:
                           return QLatin1String {};
                       }
                     }()};
    if (!name.isEmpty()) {
      item->setText(name);
    }
  }
}

bool CMyDeviceBrowser::isCollapsed(CMyDeviceBrowserItem* item)
{
  if (!item->isContainer()) {
    return false;
  }

  auto collapse {false};
  auto row {this->row(item) + 1};
  item = this->item(row);
  if (item != nullptr) {
    collapse = item->isHidden();
  }

  return collapse;
}

void CMyDeviceBrowser::setCollapsed(CMyDeviceBrowserItem::EType type,
                                    bool collapse)
{
  if (type == CMyDeviceBrowserItem::AudioContainer
      || type == CMyDeviceBrowserItem::ImageContainer
      || type == CMyDeviceBrowserItem::VideoContainer)
  {
    auto* item {this->item(type)};
    setCollapsed(item, collapse);
  }
}

void CMyDeviceBrowser::setCollapsed(CMyDeviceBrowserItem* item, bool collapse)
{
  if (!item->isContainer()) {
    return;
  }
  auto row {this->row(item) + 1};
  while ((item = this->item(row)) != nullptr && !item->isContainer()) {
    item->setHidden(collapse);
    ++row;
  }
}

bool CMyDeviceBrowser::collapseExpand(CMyDeviceBrowserItem* item)
{
  if (!item->isContainer()) {
    return false;
  }
  auto pos {viewport()->mapFromGlobal(QCursor::pos())};
  auto index {indexAt(pos)};
  auto rect {visualRect(index)};
  auto size {iconSize()};
  rect.setSize(size);
  if (!rect.contains(pos)) {
    return false;
  }

  auto row {this->row(item) + 1};
  auto nextItem {this->item(row)};
  if (nextItem == nullptr) {
    return false;
  }

  const auto collapse {!nextItem->isHidden()};
  setCollapsed(item, collapse);
  return collapse;
}

void CMyDeviceBrowser::containerDoubleClicked(CMyDeviceBrowserItem* item)
{
  if (!item->isContainer()) {
    return;
  }
  if (isCollapsed(item)) {
    setCollapsed(item, false);
  }

  auto playlistType {static_cast<CMyDeviceBrowserItem::EType>(item->type())};
  switch (playlistType) {
    case CMyDeviceBrowserItem::AudioContainer:
      playlistType = CMyDeviceBrowserItem::AudioPlaylist;
      break;

    case CMyDeviceBrowserItem::ImageContainer:
      playlistType = CMyDeviceBrowserItem::ImagePlaylist;
      break;

    default:
      playlistType = CMyDeviceBrowserItem::VideoPlaylist;
      break;
  }

  // Find the last item for the category.
  auto name {CMyDeviceBrowserItem::defaultNewPlaylistName(playlistType)};
  item = new CMyDeviceBrowserItem {::resIcon("playlist"), name, playlistType};
  name = updateName(item);
  item->setText(name);
  item->setFlags(item->flags() | Qt::ItemIsEditable | Qt::ItemIsSelectable
                 | Qt::ItemIsEnabled);
  QListWidget::addItem(item);
  setCurrentItem(item);
  editItem(item);
  emit newPlaylist(name, playlistType);
  updateContainerItemCount();
}

void CMyDeviceBrowser::commitData(QWidget* editor)
{
  auto item {static_cast<CMyDeviceBrowserItem*>(currentItem())};
  auto name {item->text()};
  QListWidget::commitData(editor);
  if (item->text().isEmpty()) {
    name = CMyDeviceBrowserItem::defaultNewPlaylistName(item->type());
    item->setText(name);
  }

  auto newName {updateName(item)};
  item->setText(newName);
  emit renamePlaylist(name, newName);
}

void CMyDeviceBrowser::delKey()
{
  const auto items {selectedItems()};
  std::ranges::for_each(
      items,
      [&](QListWidgetItem* item)
      {
        auto mdbItem {static_cast<CMyDeviceBrowserItem*>(item)};
        if (!mdbItem->isUserPlaylist()) {
          return;
        }
        const auto name {mdbItem->text()};
        const auto title {QObject::tr("Confirm playlist removing")};
        const auto btn {QMessageBox::question(this, title, name)};
        if (btn == QMessageBox::Yes) {
          emit removePlaylist(mdbItem->playlistName());
          delete item;
          updateContainerItemCount();
        }
      });
}

void CMyDeviceBrowser::rename()
{
  const auto items {selectedItems()};
  if (!items.isEmpty()) {
    auto item {items.first()};
    editItem(item);
  }
}

QString CMyDeviceBrowser::updateName(QListWidgetItem* item)
{
  auto cItems {count()};
  QStringList existingNames;
  existingNames.reserve(cItems);
  for (int iItem = 0; iItem < cItems; ++iItem) {
    auto itemPlaylist {this->item(iItem)};
    if (item != itemPlaylist
        && itemPlaylist->type() >= CMyDeviceBrowserItem::AudioFavorite)
    {
      existingNames << itemPlaylist->text();
    }
  }

  return ::updateName(item->text(), existingNames);
}

CMyDeviceBrowserItem* CMyDeviceBrowser::addItem(int type, QString const& name)
{
  auto item {new CMyDeviceBrowserItem {
      ::resIcon("playlist"),
      name,
      static_cast<CMyDeviceBrowserItem::EType>(type)}};
  item->setFlags(item->flags() | Qt::ItemIsEditable | Qt::ItemIsSelectable
                 | Qt::ItemIsEnabled);
  QListWidget::addItem(item);
  return item;
}

CMyDeviceBrowserItem* CMyDeviceBrowser::containerItem(
    CMyDeviceBrowserItem::EType type)
{
  if (!CMyDeviceBrowserItem::isContainer(type)) {
    switch (type) {
      case CMyDeviceBrowserItem::AudioFavorite:
      case CMyDeviceBrowserItem::AudioPlaylist:
        type = CMyDeviceBrowserItem::AudioContainer;
        break;

      case CMyDeviceBrowserItem::ImageFavorite:
      case CMyDeviceBrowserItem::ImagePlaylist:
        type = CMyDeviceBrowserItem::ImageContainer;
        break;

      default:
        type = CMyDeviceBrowserItem::VideoContainer;
        break;
    }
  }

  return this->item(type);
}

CMyDeviceBrowserItem* CMyDeviceBrowser::containerItem(
    CMyDeviceBrowserItem* item)
{
  if (item->isContainer()) {
    return item;
  }

  for (int iItem = row(item); iItem >= 0; --iItem) {
    auto mdbItem {static_cast<CMyDeviceBrowserItem*>(this->item(iItem))};
    if (mdbItem->isContainer()) {
      return mdbItem;
    }
  }

  return nullptr;
}

CMyDeviceBrowserItem* CMyDeviceBrowser::item(CMyDeviceBrowserItem::EType type)
{
  for (int iItem = 0, cItems = count(); iItem < cItems; ++iItem) {
    auto item {static_cast<CMyDeviceBrowserItem*>(this->item(iItem))};
    if (item != nullptr && item->type() == type) {
      return item;
    }
  }

  return nullptr;
}

void CMyDeviceBrowser::keyReturn()
{
  if (hasFocus()) {
    auto items {selectedItems()};
    if (!items.isEmpty()) {
      emit itemDoubleClicked(items.first());
    }
  }
}

void CMyDeviceBrowser::setCount(CMyDeviceBrowserItem::EType type, int count)
{
  auto item {this->item(type)};
  if (item != nullptr) {
    item->setCount(count);
  }
}

void CMyDeviceBrowser::setCount(QString const& name, int count)
{
  for (int iItem = 0, cItems = this->count(); iItem < cItems; ++iItem) {
    auto item {static_cast<CMyDeviceBrowserItem*>(this->item(iItem))};
    auto text {item->text()};
    auto itemCount {CMyDeviceBrowserItem::removeCount(text)};
    if (name == text && count != itemCount) {
      item->setCount(count);
      break;
    }
  }
}

void CMyDeviceBrowser::updateContainerItemCount()
{
  auto cAudios = 0, cImages = 0, cVideos = 0;
  for (int iItem = 0, cItems = this->count(); iItem < cItems; ++iItem) {
    auto item {static_cast<CMyDeviceBrowserItem*>(this->item(iItem))};
    switch (item->type()) {
      case CMyDeviceBrowserItem::AudioPlaylist:
      case CMyDeviceBrowserItem::AudioFavorite:
        ++cAudios;
        break;

      case CMyDeviceBrowserItem::ImagePlaylist:
      case CMyDeviceBrowserItem::ImageFavorite:
        ++cImages;
        break;

      case CMyDeviceBrowserItem::VideoPlaylist:
      case CMyDeviceBrowserItem::VideoFavorite:
        ++cVideos;
        break;

      default:
        break;
    }
  }

  auto item {this->item(CMyDeviceBrowserItem::AudioContainer)};
  item->setCount(cAudios);

  item = this->item(CMyDeviceBrowserItem::ImageContainer);
  item->setCount(cImages);

  item = this->item(CMyDeviceBrowserItem::VideoContainer);
  item->setCount(cVideos);
}
