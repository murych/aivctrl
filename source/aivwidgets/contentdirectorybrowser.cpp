#include <QRandomGenerator>
#include <QScrollBar>
#include <QShortcut>

#include "contentdirectorybrowser.hpp"

#include "pixmapcache.hpp"
#include "plugin.hpp"
#include "widgethelper.hpp"

using namespace QtUPnP;

int CContentDirectoryBrowserItem::m_albumArtURITimeout {10000};
QTimer CContentDirectoryBrowser::m_iconUpdateTimer;
int const CContentDirectoryBrowser::m_iconUpdateThresholdTime {500};
int const CContentDirectoryBrowser::m_iconUpdateRepeatTime {10};
CContentDirectoryBrowser* CContentDirectoryBrowser::m_directoryBrowser {
    nullptr};

CContentDirectoryBrowserItem::CContentDirectoryBrowserItem(QListWidget* parent,
                                                           int type)
    : QListWidgetItem {parent, type}
{
}

CContentDirectoryBrowserItem::CContentDirectoryBrowserItem(
    const CDidlItem& didlItem, QListWidget* parent, int type)
    : QListWidgetItem {parent, type}
    , m_didlItem {didlItem}
{
  const auto label {didlItem.title()};
  setText(label);
  updateTooltip();
}

CContentDirectoryBrowserItem::CContentDirectoryBrowserItem(
    CContentDirectoryBrowserItem const& other)
    : QListWidgetItem {other}
    , m_didlItem {other.m_didlItem}
{
  updateTooltip();
  m_iconType = IconNotdefined;
}

QPixmap CContentDirectoryBrowserItem::standardIcon(
    CDidlItem const& didlItem, QStringList const& defaultPixmaps)
{
  QPixmap pxm;
  CDidlItem::EType type {didlItem.type()};
  if (type < defaultPixmaps.size()) {
    pxm.load(::resIconFullPath(defaultPixmaps.at(type)));
  }

  return pxm;
}

void CContentDirectoryBrowserItem::updateIcon(CControlPoint* cp,
                                              CPixmapCache* cache,
                                              int iconIndex,
                                              QStringList const& defaultPixmaps,
                                              QSize const& iconSize)
{
  if (iconType() != CContentDirectoryBrowserItem::IconNotdefined) {
    return;
  }
  QPixmap pxm;
  const auto uri {m_didlItem.albumArtURI(iconIndex)};
  if (!uri.isEmpty() && cache != nullptr) {
    pxm = cache->search(uri);
  }

  if (pxm.isNull()) {
    if (!uri.isEmpty()) {
      emit cp->networkComStarted(CDevice::MediaServer);
    }

    const auto pxmBytes {cp->callData(uri, m_albumArtURITimeout)};
    if (!uri.isEmpty()) {
      emit cp->networkComEnded(CDevice::MediaServer);
    }

    if (!pxmBytes.isEmpty()) {
      if (cache != nullptr) {
        cache->add(uri, pxmBytes, iconSize);
        pxm = cache->search(uri);
      } else {
        pxm.loadFromData(pxmBytes);
      }

      m_iconType = IconServer;
    } else {
      pxm = CContentDirectoryBrowserItem::standardIcon(m_didlItem,
                                                       defaultPixmaps);
      m_iconType = IConStandard;
    }
  } else {
    m_iconType = IconServer;
  }

  setIcon(QIcon {pxm});
}

void CContentDirectoryBrowserItem::updateTooltip()
{
  const auto string {m_didlItem.isEmpty() ? QObject::tr("Unknown")
                                          : m_didlItem.title()};

  QString tooltip = TOOLTIPFIELD1("Title", string);
  tooltip += TOOLTIPFIELD("Album", m_didlItem.album());
  tooltip += TOOLTIPFIELD("Date", m_didlItem.date());
  tooltip += TOOLTIPFIELD("Artist", m_didlItem.artist());
  tooltip += TOOLTIPFIELD("Artist album", m_didlItem.albumArtist());
  tooltip += TOOLTIPFIELD("Composer", m_didlItem.composer());
  tooltip += TOOLTIPFIELD("Creator", m_didlItem.creator());
  tooltip += TOOLTIPFIELD("Genre", m_didlItem.genre());
  tooltip += TOOLTIPFIELD("Resolution", m_didlItem.resolution());
  tooltip += TOOLTIPFIELD("Size (kB)", m_didlItem.size() / 1024);
  tooltip += TOOLTIPFIELD("Bitrate (kbs)", m_didlItem.bitrate() * 8 / 1024);
  tooltip += TOOLTIPFIELD("Duration", m_didlItem.duration());
  tooltip += TOOLTIPFIELD("Audio channels", m_didlItem.nrAudioChannels());
  tooltip +=
      TOOLTIPFIELD("Sample frequency (khz)",
                   static_cast<double>(m_didlItem.sampleFrequency()) / 1000);
  setToolTip(tooltip);
}

CContentDirectoryBrowser::CContentDirectoryBrowser(QWidget* parent)
    : CListWidgetBase {parent}
{
  auto* sb {verticalScrollBar()};
  connect(sb,
          &QScrollBar::valueChanged,
          this,
          &CContentDirectoryBrowser::scrollBarValueChanged);
  connect(this,
          &QListWidget::itemSelectionChanged,
          this,
          [&]
          {
            m_cSelected = 0;
            for (int iItem = 0, cItems = count(); iItem < cItems; ++iItem) {
              auto* item {this->item(iItem)};
              if (item->isSelected()) {
                ++m_cSelected;
              }
            }
          });
  connect(new QShortcut {QKeySequence(Qt::Key_Escape), this},
          &QShortcut::activated,
          this,
          &CContentDirectoryBrowser::clearSelection);
  connect(new QShortcut {QKeySequence(Qt::Key_Return), this},
          &QShortcut::activated,
          this,
          &CContentDirectoryBrowser::startStream);
  m_iconUpdateTimer.setInterval(m_iconUpdateThresholdTime);
  if (!m_iconUpdateTimer.isSingleShot()) {
    m_iconUpdateTimer.setSingleShot(true);
  }
}

int CContentDirectoryBrowser::addItems(QList<CDidlItem> const& didlItems)
{
  if (didlItems.isEmpty()) {
    return didlItems.size();
  }

  std::ranges::for_each(
      didlItems,
      [this](const CDidlItem& didlItem)
      {
        auto* item {new CContentDirectoryBrowserItem {didlItem, this}};
        item->setIcon(CContentDirectoryBrowserItem::standardIcon(
            didlItem, m_defaultIcons));
      });

  startIconUpdateTimer();

  return didlItems.size();
}

int CContentDirectoryBrowser::addItems(QString const& server,
                                       QString const& id,
                                       CContentDirectory::EBrowseType type,
                                       QString filter,
                                       int startingIndex,
                                       int requestedCount,
                                       QString const& sortCriteria)
{
  if (filter.isEmpty()) {
    filter = QLatin1Char('*');
  }

  clear();
  auto reply {
      [&]() -> CBrowseReply
      {
        auto plugin {m_cp->plugin(server)};
        if (plugin != nullptr) {
          return plugin->browse(
              id, type, filter, startingIndex, requestedCount, sortCriteria);
        }

        CContentDirectory cd {m_cp};
        return cd.browse(server,
                         id,
                         type,
                         filter,
                         startingIndex,
                         requestedCount,
                         sortCriteria);
      }()};

  auto numberReturned {static_cast<int>(reply.numberReturned())};
  if (numberReturned == 0) {
    return numberReturned;
  }

  if (sortCriteria == QString {} && m_sortCriteria != QString {}) {
    reply.sort(m_sortCriteria, m_sortDir);
  }

  addItems(reply.items());

  return numberReturned;
}

void CContentDirectoryBrowser::updateIcon()
{
  stopIconUpdateTimer();
  if (m_blockIconUpdate) {
    return;
  }
  if (count() == 0) {
    return;
  }
  auto itemsRect {viewport()->rect()};
  itemsRect.adjust(-10, -10, 10, 10);
  const auto offset {iconSize() / 2};
  const auto xOffset {offset.width()};
  const auto yOffset {offset.height()};
  const auto top {indexAt(itemsRect.topLeft() + QPoint {xOffset, yOffset})};
  if (!top.isValid()) {
    return;
  }

  auto firstRow {top.row()};
  if (firstRow > 0) {
    --firstRow;
  }

  auto update {false};
  const auto cItems {count()};
  const auto bottom {
      indexAt(itemsRect.bottomLeft() + QPoint {xOffset, -yOffset})};
  auto lastRow {bottom.isValid() ? bottom.row() : cItems - 1};
  auto iconSize {this->iconSize()};
  for (int iItem = firstRow; iItem <= lastRow; ++iItem) {
    auto* item {static_cast<CContentDirectoryBrowserItem*>(this->item(iItem))};
    if (item->iconType() == CContentDirectoryBrowserItem::IconNotdefined) {
      auto itemRect {visualItemRect(item)};
      if (itemsRect.intersects(itemRect)) {
        update = true;
        item->updateIcon(m_cp, m_pixmapCache, -1, m_defaultIcons, iconSize);
        break;
      }
    }
  }

  if (update) {
    m_iconUpdateTimer.setInterval(m_iconUpdateRepeatTime);
    m_iconUpdateTimer.start();
  }
}

void CContentDirectoryBrowser::resizeEvent(QResizeEvent*)
{
  startIconUpdateTimer();
}

void CContentDirectoryBrowser::scrollBarValueChanged(int)
{
  startIconUpdateTimer();
}

void CContentDirectoryBrowser::startIconUpdateTimer()
{
  stopIconUpdateTimer();
  if (m_blockIconUpdate && count() == 0) {
    return;
  }

  if (m_directoryBrowser != this) {
    if (m_directoryBrowser != nullptr) {
      disconnect(&m_iconUpdateTimer,
                 &QTimer::timeout,
                 this,
                 &CContentDirectoryBrowser::updateIcon);
    }

    connect(&m_iconUpdateTimer,
            &QTimer::timeout,
            this,
            &CContentDirectoryBrowser::updateIcon);
    m_directoryBrowser = this;
  }

  m_iconUpdateTimer.setInterval(m_iconUpdateThresholdTime);
  m_iconUpdateTimer.start();
}

void CContentDirectoryBrowser::stopIconUpdateTimer()
{
  m_iconUpdateTimer.stop();
}

void CContentDirectoryBrowser::setBold(QString const& uri)
{
  if (uri.isEmpty()) {
    return;
  }
  for (int iItem = 0, end = count(); iItem < end; ++iItem) {
    auto* item {static_cast<CContentDirectoryBrowserItem*>(this->item(iItem))};
    const auto didl {item->didlItem()};
    auto font {item->font()};
    auto didURI {replace127_0_0_1(didl.uri(0))};
    if (!didURI.isEmpty() && uri.endsWith(didURI)) {
      // endsWith for plugins.
      if (!font.bold()) {
        font.setBold(true);
        item->setFont(font);
        scrollToItem(item);
      }
    } else if (font.bold()) {
      font.setBold(false);
      item->setFont(font);
    }
  }
}

CDidlItem CContentDirectoryBrowser::didlItem(const QString& uri) const
{
  const auto didlItem {
      [&]() -> CDidlItem
      {
        for (auto iItem = 0, end = count(); iItem < end; ++iItem) {
          auto item {
              static_cast<CContentDirectoryBrowserItem*>(this->item(iItem))};
          const auto didl {item->didlItem()};
          const auto uris {didl.uris()};
          if (std::find(uris.cbegin(), uris.cend(), uri) != uris.end()) {
            return didl;
          }
        }
        return CDidlItem {};
      }()};

  return didlItem;
}

int CContentDirectoryBrowser::nextIndex(bool forward, bool repeat, bool shuffle)
{
  const auto cItems {count()};
  if (shuffle) {
    const auto r {QRandomGenerator::global()->generate()};
    return r % cItems;
  }

  auto index {boldIndex()};
  if (forward) {
    ++index;
    if (index >= cItems) {
      index = repeat ? 0 : -1;
    }
  } else {
    --index;
    if (index < 0) {
      index = repeat ? cItems - 1 : -1;
    }
  }

  return index;
}

void CContentDirectoryBrowser::startStream()
{
  if (!selectedItems().isEmpty()) {
    emit itemDoubleClicked(selectedItems().first());
  }
}

void CContentDirectoryBrowser::setIconType(
    CContentDirectoryBrowserItem::EIconType type)
{
  for (auto iItem = 0, cItems = count(); iItem < cItems; ++iItem) {
    auto item {static_cast<CContentDirectoryBrowserItem*>(this->item(iItem))};
    item->setIconType(type);
  }
}
