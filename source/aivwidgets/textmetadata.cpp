#include "textmetadata.hpp"

CTextMetaData::CTextMetaData(QWidget* parent)
    : QTextEdit {parent}
{
  const auto fontMetrics {this->fontMetrics()};
  const auto spacing {fontMetrics.lineSpacing()};
  setMaximumHeight(4 * spacing);
  setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
}

QString CTextMetaData::truncateText(QString const& text)
{
  return truncateText(text, this);
}

QString CTextMetaData::truncateText(QString const& text, QWidget* widget)
{
  if (text.isEmpty() && !(text.size() > 1 || text.at(0) != ' ')) {
    return text;
  }

  const auto fontMetrics {widget->fontMetrics()};
  const auto textWidth {fontMetrics.horizontalAdvance(text, -1)};
  const auto width {17 * widget->width() / 20};  // 15%
  if (textWidth < width) {
    return text;
  }

  auto truncatedText {text};
  auto length {truncatedText.length()};
  const auto suspensionPoint {"..."};
  truncatedText += suspensionPoint;
  while (length > 0
         && fontMetrics.horizontalAdvance(truncatedText, -1) >= width)
  {
    truncatedText = truncatedText.left(--length) + suspensionPoint;
  }

  return truncatedText;
}

void CTextMetaData::resizeEvent(QResizeEvent* event)
{
  QTextEdit::resizeEvent(event);
  updateText();
}

void CTextMetaData::updateText()
{
  const auto lf {QStringLiteral("<br/>")};
  auto html {QStringLiteral("<p style=\"text-align:center\">")};
  if (m_texts.isEmpty()) {
    m_texts << " ";
  } else if (m_texts.first().isEmpty()) {
    m_texts.first() = ' ';
  }

  std::ranges::for_each(m_texts,
                        [&](const auto& text)
                        {
                          if (text != m_texts.first()) {
                            html += lf;
                          }
                          html += truncateText(text);
                        });

  html += "</p>";
  QTextEdit::setHtml(html);
}

void CTextMetaData::setText(QStringList const& texts)
{
  m_texts = texts;
  updateText();
}

void CTextMetaData::clear()
{
  m_texts.clear();
  QTextEdit::clear();
}
