#include <QMenu>

#include "mainwindow.hpp"

#include "aivwidgets/networkprogress.hpp"
#include "aivwidgets/widgethelper.hpp"
#include "avtransport.hpp"
#include "helper.hpp"
#include "playlist.hpp"
#include "plugin.hpp"
#include "session.hpp"
#include "ui_mainwindow.h"

namespace
{
const std::unordered_map<int, QString> playModeList {
    {CMainWindow::Normal, QStringLiteral("NORMAL")},
    {CMainWindow::RepeatAll, QStringLiteral("REPEAT_ALL")},
    {CMainWindow::RepeatOne, QStringLiteral("REPEAT_ONE")},
    {CMainWindow::Shuffle, QStringLiteral("SHUFFLE")},
    {CMainWindow::Random, QStringLiteral("RANDOM")}};
}

using namespace QtUPnP;

void CMainWindow::updateContentDirectory(CFolderItem const& item, bool parentID)
{
  QApplication::setOverrideCursor(Qt::WaitCursor);
  const auto id {parentID ? item.parentID() : item.id()};
  if (!id.isEmpty()) {
    ui->m_contentDirectory->addItems(m_server, id);
  }

  item.setListWidgetCurrentItem();
  QApplication::restoreOverrideCursor();
}

void CMainWindow::muteIcon(bool mute)
{
  const auto iconName {mute ? "speaker_off" : "speaker_on"};
  const auto icon {::resIcon(iconName)};
  ui->m_mute->setIcon(icon);
  ui->m_mute2->setIcon(icon);
}

void CMainWindow::playingIcon(bool playing)
{
  const auto iconName {playing ? "playing_on" : "playing_off"};
  const auto icon {::resIcon(iconName)};
  ui->m_play->setIcon(icon);
  ui->m_play2->setIcon(icon);
}

void CMainWindow::updateVolumeSlider(int volume, bool blocked)
{
  if (blocked) {
    ui->m_volume->blockSignals(blocked);
    ui->m_volume2->blockSignals(blocked);
  }

  ui->m_volume->setValue(volume);
  ui->m_volume2->setValue(volume);

  if (blocked) {
    ui->m_volume->blockSignals(false);
    ui->m_volume2->blockSignals(false);
  }
}

void CMainWindow::updateCurrentPlayling(const CDidlItem& didl,
                                        bool startPlaying,
                                        bool fullUpdate)
{
  const auto title {didl.title()};
  if (fullUpdate || !title.isEmpty()) {
    ui->m_title->setText(title);
    ui->m_title->setToolTip(title);

    QStringList texts;
    texts << title;

    const auto album {didl.album()};
    if (!album.isEmpty()) {
      texts << album;
    }

    auto artist {didl.artist()};
    if (artist.isEmpty()) {
      artist = didl.albumArtist();
    }

    if (!artist.isEmpty()) {
      texts << artist;
    }

    const auto iconName {isFavorite(m_playlists, didl) ? "favorite_true"
                                                       : "favorite"};
    ui->m_favorite->setIcon(::resIcon(iconName));

    ui->m_trackMetadata->setText(texts);
    if (startPlaying) {
      const auto duration {didl.duration()};
      if (::timeToMS(duration) != 0) {
        startPositionTimer();
      }
    }
  }

  const auto albumArtURI {didl.albumArtURI(0)};
  if (fullUpdate || !albumArtURI.isEmpty()) {
    ui->m_cover->setImage(albumArtURI);
    ui->m_currentPlayingIcon->setIcon(QIcon {ui->m_cover->image()});
  }
}

void CMainWindow::clearCurrentPlayling()
{
  ui->m_title->clear();
  ui->m_title->setToolTip({});

  ui->m_cover->setImage({});
  ui->m_currentPlayingIcon->setIcon(QIcon {ui->m_cover->image()});
  ui->m_trackMetadata->clear();
}

void CMainWindow::updateCurrentPlayling(
    CContentDirectoryBrowserItem const* item,
    bool startPlaying,
    bool fullUpdate)
{
  const auto didl {item->didlItem()};
  updateCurrentPlayling(didl, startPlaying, fullUpdate);
}

void CMainWindow::startPositionTimer()
{
  if (!m_positionTimer.isActive()) {
    m_relTimeCurrent = 0;
    m_positionTimer.start();
  }
}

void CMainWindow::stopPositionTimer()
{
  if (m_positionTimer.isActive()) {
    m_relTimeCurrent = 0;
    m_positionTimer.stop();
  }
}

void CMainWindow::togglePositionTimer(bool playing)
{
  if (playing) {
    startPositionTimer();
  } else {
    stopPositionTimer();
  }
}

void CMainWindow::updatePP(bool playing)
{
  ui->m_play->setEnabled(true);
  ui->m_play2->setEnabled(true);
  togglePositionTimer(playing);
  playingIcon(playing);
}

void CMainWindow::setItemBold(const QString& uri)
{
  if (!uri.isEmpty()) {
    ui->m_contentDirectory->setBold(uri);
    ui->m_queue->setBold(uri);
    ui->m_playlistContent->setBold(uri);
  }
}

void CMainWindow::setItemBold(CContentDirectoryBrowserItem const* item)
{
  setItemBold(item->didlItem());
}

void CMainWindow::setItemBold(const CDidlItem& item)
{
  const auto uri {item.uri(0)};
  setItemBold(uri);
}

void CMainWindow::setAVTransport(CContentDirectoryBrowserItem const* item)
{
  stopPositionTimer();
  ui->m_queue->setAVTransportURI(
      m_cp.get(), m_renderer, item->listWidget()->row(item));
  setItemBold(item);
  playingIcon(true);
  updateCurrentPlayling(item, true, true);
}

bool CMainWindow::nextItem(bool forward)
{
  const auto index {
      ui->m_queue->nextIndex(forward,
                             m_playMode == RepeatAll,
                             m_playMode == Shuffle || m_playMode == Random)};
  if (index == -1) {
    return false;
  }

  const auto item {ui->m_queue->item(index)};
  on_m_queue_itemDoubleClicked(item);
  return true;
}

void CMainWindow::applyPlayMode()
{
  auto repeatIconName {"repeat_off"};
  auto shuffleIconName {"shuffle_off"};
  switch (m_playMode) {
    case RepeatOne:
      repeatIconName = "repeat_one";
      break;

    case RepeatAll:
      repeatIconName = "repeat_all";
      break;

    case Shuffle:
    case Random:
      shuffleIconName = "shuffle_on";
      break;

    default:
      break;
  }

  ui->m_repeat->setIcon(::resIcon(repeatIconName));
  ui->m_shuffle->setIcon(::resIcon(shuffleIconName));
}

QString CMainWindow::playModeString()
{
  const auto device {m_cp->device(m_renderer)};
  const auto currentPlayMode {device.stateVariable("CurrentPlayMode", {})};
  const auto playModes {currentPlayMode.allowedValues()};

  std::unordered_map<int, QString> map;
  for (int iPlayMode = Normal; iPlayMode < LastPlayMode; ++iPlayMode) {
    const auto validPlayMode {
        [&]() -> int
        {
          auto begin {playModes.cbegin()};
          auto end {playModes.cend()};
          if (std::find(begin, end, playModeList.at(iPlayMode)) != end) {
            return iPlayMode;
          }

          if (iPlayMode == Shuffle
              && std::find(begin, end, playModeList.at(Random)) != end)
          {
            return Random;
          }

          if (iPlayMode == Random
              && std::find(begin, end, playModeList.at(Random)) != end)
          {
            return Shuffle;
          }

          if (iPlayMode == RepeatOne
              && std::find(begin, end, playModeList.at(RepeatOne)) == end)
          {
            return Normal;
          }

          if (iPlayMode == RepeatAll
              && std::find(begin, end, playModeList.at(RepeatAll)) == end)
          {
            return Normal;
          }

          return Normal;
        }()};

    map.emplace(iPlayMode, playModeList.at(validPlayMode));
  }

  return map.at(m_playMode);
}

CMainWindow::EPlayMode CMainWindow::playMode(
    const QString& currentPlayModeString)
{
  const auto iter {std::find_if(
      playModeList.cbegin(),
      playModeList.cend(),
      [&](const auto& it) { return it.second == currentPlayModeString; })};
  if (iter != playModeList.end()) {
    return static_cast<EPlayMode>(iter->first);
  }

  return Normal;
}

void CMainWindow::applyLastSession()
{
  CSession session;
  if (session.restore()) {
    session.setGeometry(this);
    m_renderer = session.renderer();
    m_language = session.language();
    m_status.setStatus(session.status());
    const auto size {session.iconSize()};
    m_iconSize = QSize {size, size};
  } else {
    m_status.addStatus(ShowNetworkCom);
  }

  ui->m_queue->setDisableUPnPPlaylist(m_status.hasStatus(UPnPPlaylistDisabled));

  auto* controlPointRenderer {
      new CNetworkProgress("m_controlPointRenderer", this)};
  ui->m_comNetworkLayout->insertWidget(2, controlPointRenderer);

  auto* controlPointServer {new CNetworkProgress("m_controlPointServer", this)};
  controlPointServer->setInverted(true);
  ui->m_comNetworkLayout->insertWidget(1, controlPointServer);

  m_cp->validateNetworkCom(true);

  const auto font {ui->m_servers->font()};
  controlPointRenderer->setFont(font);
  controlPointServer->setFont(font);
  controlPointRenderer->setType(CNetworkProgress::Binary);
  controlPointServer->setType(CNetworkProgress::Binary);

  ui->m_networkProgress->setHidden(!m_status.hasStatus(ShowNetworkCom));
  m_playMode = static_cast<EPlayMode>(session.playMode());
  applyPlayMode();
  restorePlaylists(m_playlists);
  for (auto it = m_playlists.cbegin(), end = m_playlists.cend(); it != end;
       ++it)
  {
    const auto playlist {it.value()};
    const auto type {playlist.type()};
    if (type != CPlaylist::AudioFavorites && type != CPlaylist::ImageFavorites
        && type != CPlaylist::VideoFavorites)
    {
      ui->m_myDevice->addItem(type, it.key());
    }
  }

  m_translator = installTranslator(m_language);

  using TCollapse = std::pair<EStatus, CMyDeviceBrowserItem::EType>;
  constexpr std::array<TCollapse, 3> collapses {
      std::make_pair(CollapseAudioPlaylist,
                     CMyDeviceBrowserItem::AudioContainer),
      std::make_pair(CollapseImagePlaylist,
                     CMyDeviceBrowserItem::ImageContainer),
      std::make_pair(CollapseVideoPlaylist,
                     CMyDeviceBrowserItem::VideoContainer),
  };
  std::ranges::for_each(collapses,
                        [&](const auto& collapse)
                        {
                          if (m_status.hasStatus(collapse.first)) {
                            ui->m_myDevice->setCollapsed(collapse.second, true);
                          }
                        });

  updatePlaylistItemCount();

  const QDir dir {::appDataDirectory()};
  const QStringList uuids {m_cp->plugins()};
  std::ranges::for_each(
      uuids,
      [&](const auto& uuid)
      {
        const auto plugin {m_cp->plugin(uuid)};
        const auto fileName {dir.absoluteFilePath(plugin->name() + ".ids")};
        plugin->restoreAuth(fileName);
      });

  if (!m_status.hasStatus(ShowCloudServers)) {
    setCloudServersHidden(true);
  }
}

void CMainWindow::rotateIcon()
{
  auto angle {m_iconAngle};
  if (angle == 30) {
    m_iconAngle = -15;
  }

  m_iconAngle += 15;
  QString iconName {"discovery"};
  iconName += QString::number(angle);
  ui->m_home->setIcon(::resIcon(iconName));
}

int CMainWindow::didlItems(QList<CDidlItem>& items, CDidlItem const& item)
{
  if (item.isContainer()) {
    const auto id {item.containerID()};
    CContentDirectory cd {m_cp.get()};
    const auto reply {cd.browse(m_server, id)};
    const auto replyItems {reply.items()};
    std::ranges::for_each(
        replyItems,
        [&](const auto& replyItem)
        {
          if (replyItem.isContainer()) {
            const auto text {tr("Scan: ") + replyItem.title()};
            ui->m_provider->setText(text);
          }

          didlItems(items, replyItem);
        });
  } else {
    items.push_back(item);
  }

  return items.size();
}

QList<CDidlItem> CMainWindow::didlItems(QList<QListWidgetItem*> lwItems)
{
  QGuiApplication::setOverrideCursor(Qt::WaitCursor);
  const auto count {lwItems.size() + 30};
  QList<CDidlItem> items;
  items.reserve(count);
  std::ranges::for_each(
      lwItems,
      [&](auto* lwItem)
      {
        auto* cdItem {static_cast<CContentDirectoryBrowserItem*>(lwItem)};
        didlItems(items, cdItem->didlItem());
      });

  QGuiApplication::restoreOverrideCursor();
  return items;
}

void CMainWindow::currentQueueChanged()
{
  // Queue empty, make nothing
  if (ui->m_queue->count() == 0) {
    return;
  }
  const auto index {ui->m_queue->boldIndex()};
  m_cp->clearPlaylist();
  if (index >= 0)
  {  // The bold index is not removed. Continue at the same position.
    if (!m_cp->playlistName().isEmpty())
    {  // In case of playlist, reload the playlist en continue at the same
       // position.
      auto* item {ui->m_queue->item(index)};
      auto position {CAVTransport {m_cp.get()}.getPositionInfo(m_renderer)};
      on_m_queue_itemDoubleClicked(item);
      const auto relTime {position.relTime()};
      on_m_position_valueChanged(::timeToS(relTime));
    }
  } else {  // The bold index is removed. Start at the position 0.
    auto* item {ui->m_queue->item(0)};
    on_m_queue_itemDoubleClicked(item);
  }
}

void CMainWindow::updatePlaylistItemCount()
{
  ui->m_myDevice->updateContainerItemCount();
  for (auto it = m_playlists.cbegin(), end = m_playlists.cend(); it != end;
       ++it)
  {
    const auto playlist {it.value()};
    const auto type {playlist.type()};
    const auto count {playlist.items().size()};
    if (type == CPlaylist::AudioFavorites || type == CPlaylist::ImageFavorites
        || type == CPlaylist::VideoFavorites)
    {
      ui->m_myDevice->setCount(static_cast<CMyDeviceBrowserItem::EType>(type),
                               count);
    } else {
      ui->m_myDevice->setCount(it.key(), count);
    }
  }
}

void CMainWindow::updateDevicesCount(int* cServers, int* cRenderers)
{
  int cServersTmp, cRenderersTmp;
  if (cServers == nullptr) {
    cServers = &cServersTmp;
  }

  if (cRenderers == nullptr) {
    cRenderers = &cRenderersTmp;
  }

  if (ui->m_stackedWidget->currentIndex() == Home) {
    *cServers = m_cp->serversCount();
    *cRenderers = m_cp->renderersCount();
    QString text = QString::number(*cServers) + ' '
        + (*cServers <= 1 ? tr("Server") : tr("Servers")) + tr(" and ")
        + QString::number(*cRenderers) + ' '
        + (*cRenderers <= 1 ? tr("Renderer") : tr("Renderers"));
    ui->m_provider->setText(text);
  }
}

void CMainWindow::searchAction(bool activate)
{
  static QString providerText;
  if (activate) {
    providerText = ui->m_provider->text();
    ui->m_provider->clear();
    ui->m_provider->setReadOnly(false);
    ui->m_provider->setFocus();
  } else if (!ui->m_provider->isReadOnly()) {
    ui->m_provider->setReadOnly(true);
    ui->m_provider->setText(providerText);
  }
}

CNetworkProgress* CMainWindow::networkProgress(CDevice::EType type)
{
  const auto name {type == CDevice::MediaRenderer
                       ? QStringLiteral("m_controlPointRenderer")
                       : QStringLiteral("m_controlPointServer")};
  return ui->m_networkProgress->findChild<CNetworkProgress*>(name);
}

void CMainWindow::setComRendererIcon()
{
  auto* menu {ui->m_renderer->menu()};
  const auto actions {menu->actions()};
  const auto action {std::ranges::find_if(actions,
                                          [&](const QAction* action)
                                          {
                                            const auto renderer {
                                                action->data().toString()};
                                            return renderer == m_renderer;
                                          })};
  const auto icon {action == actions.end() ? QIcon {} : (*action)->icon()};
  const auto pxm {icon.pixmap(ui->m_comRenderer->size()).isNull()
                      ? QPixmap {::resIconFullPath("device")}
                      : icon.pixmap(ui->m_comRenderer->size())};

  ui->m_comRenderer->setPixmap(pxm);
}

void CMainWindow::setComServerIcon()
{
  const auto* plugin {m_cp->plugin(m_server)};
  auto pxm {plugin == nullptr
                ? ui->m_servers->icon(m_server).pixmap(ui->m_comServer->size())
                : plugin->pixmap()};

  if (pxm.isNull()) {
    pxm = QPixmap {::resIconFullPath("server")};
  }

  ui->m_comServer->setPixmap(pxm);
}

void CMainWindow::loadPlugins()
{
  const auto uuids {m_cp->plugins()};
  std::ranges::for_each(uuids,
                        [&](const auto& uuid)
                        {
                          const auto plugin {m_cp->plugin(uuid)};
                          ui->m_cloud->addItem(plugin);
                        });
}

void CMainWindow::setCloudServersHidden(bool hide)
{
  ui->m_cloud->setHidden(hide);
  ui->m_cloudLabel->setHidden(hide);
  ui->m_cloudLine1->setHidden(hide);
  ui->m_cloudLine2->setHidden(hide);
}
