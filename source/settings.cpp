#include <QListWidget>
#include <QToolButton>

#include "settings.hpp"

#include "aivwidgets/widgethelper.hpp"
#include "ui_settings.h"

CSettings::CSettings(std::bitset<LastIndex> flags,
                     QSize& iconSize,
                     QWidget* parent)
    : QDialog {parent}
    , ui {std::make_unique<Ui::CSettings>()}
    , m_flags {flags}
    , m_iconSize {iconSize}
{
  ui->setupUi(this);
  removeWindowContextHelpButton(this);

  ui->m_networkCom->setChecked(flags.test(ShowNetworkCom));
  ui->m_search->setChecked(flags.test(UseSearchForCheckPlaylist));
  ui->m_eventOnly->setChecked(flags.test(UPnPEventsOnly));
  ui->m_playlists->setChecked(flags.test(DontUsePlaylists));
  ui->m_cloudServers->setChecked(flags.test(ShowCloudServers));
  ui->m_iconSize->setValue(iconSize.width());
  m_iconSizeBackup = iconSize;
  ui->m_size->setText(QString::number(iconSize.width()));
}

CSettings::~CSettings() = default;

void CSettings::on_m_iconSize_valueChanged(int value)
{
  auto* mw {static_cast<QWidget*>(parent())};
  m_iconSize = QSize {value, value};
  ui->m_size->setText(QString::number(value));
  setIconSize(mw, m_iconSize);
  m_reset = false;
}

void CSettings::setIconSize(QWidget* w, const QSize& size)
{
  const auto tbs {w->findChildren<QToolButton*>()};
  std::ranges::for_each(tbs, [size](auto* tb) { tb->setIconSize(size); });

  const auto listWidgets {w->findChildren<QListWidget*>()};
  std::ranges::for_each(listWidgets,
                        [size](auto* list_widget)
                        {
                          list_widget->setIconSize(size);
                          list_widget->setSpacing(1);
                        });
}

void CSettings::on_m_reset_clicked()
{
  ui->m_networkCom->setChecked(true);
  ui->m_search->setChecked(false);
  ui->m_eventOnly->setChecked(false);
  ui->m_playlists->setChecked(false);
  ui->m_cloudServers->setChecked(false);
  ui->m_iconSize->setValue(32);
  m_reset = true;
}

void CSettings::on_m_ok_clicked()
{
  m_flags[ShowNetworkCom] = ui->m_networkCom->isChecked();
  m_flags[UseSearchForCheckPlaylist] = ui->m_search->isChecked();
  m_flags[UPnPEventsOnly] = ui->m_eventOnly->isChecked();
  m_flags[DontUsePlaylists] = ui->m_playlists->isChecked();
  m_flags[ShowCloudServers] = ui->m_cloudServers->isChecked();
  accept();
}

void CSettings::on_m_cancel_clicked()
{
  on_m_iconSize_valueChanged(m_iconSizeBackup.width());
  m_reset = false;
  reject();
}
