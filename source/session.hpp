#ifndef SESSION_HPP
#define SESSION_HPP

#include <QRect>
#include <QString>

#include "xmlh.hpp"

class CSession : public QtUPnP::CXmlH
{
public:
  CSession() = default;
  CSession(const QString& renderer,
           int volume,
           int playMode,
           bool remainingTime,
           const QRect& rect,
           Qt::WindowStates windowStates,
           int iconSize,
           const QString& language,
           unsigned status);
  ~CSession() override = default;

  [[nodiscard]] auto characters(const QString& name) -> bool override;

  void save(QString fileName = {});
  auto restore(QString fileName = {}) -> bool;

  [[nodiscard]] auto standardFilePath() -> QString;

  [[nodiscard]] auto renderer() const { return m_renderer; }
  [[nodiscard]] auto volume() const { return m_volume; }
  [[nodiscard]] auto playMode() const { return m_playMode; }
  [[nodiscard]] auto remainingTime() const { return m_remainingTime; }
  [[nodiscard]] auto geometry() const { return m_rect; }
  [[nodiscard]] auto windowStates() const { return m_windowStates; }
  [[nodiscard]] auto iconSize() const { return m_iconSize; }
  [[nodiscard]] auto language() const { return m_language; }
  [[nodiscard]] auto status() const { return m_status; }

  void setGeometry(QWidget* widget) const;

protected:
  QString m_renderer;
  int m_volume {0};
  int m_playMode {0};
  bool m_remainingTime {false};
  QRect m_rect;
  Qt::WindowStates m_windowStates {Qt::WindowNoState};
  int m_iconSize {32};
  QString m_language;
  unsigned m_status {0};
  const QString m_fileName {"session.xml"};
};

#endif  // SESSION_HPP
